#include "TrkAnalysisSteppingAction.hh"
#include "G4Step.hh"
#include "G4Track.hh"
#include "G4UnitsTable.hh"
#include "G4AnalysisManager.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"
#include "TrkAnalysisRunAction.hh"
#include "G4VPhysicalVolume.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrkAnalysisSteppingAction::TrkAnalysisSteppingAction(): G4UserSteppingAction()
{

}
 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrkAnalysisSteppingAction::~TrkAnalysisSteppingAction()
{
    // ---
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TrkAnalysisSteppingAction::UserSteppingAction(const G4Step * aStep)
{
    G4ThreeVector postStepPoint = aStep->GetPostStepPoint()->GetPosition();
    G4float postTime = aStep->GetPostStepPoint()->GetGlobalTime();

    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

    TrkAnalysisRunAction::SetStepPointX(postStepPoint.x());
    TrkAnalysisRunAction::SetStepPointY(postStepPoint.y());
    TrkAnalysisRunAction::SetStepPointZ(postStepPoint.z());
    TrkAnalysisRunAction::SetStepPointTime(postTime);



    //the lines below were written to special simulation, one can remove them
    G4VPhysicalVolume* pVol = aStep->GetPostStepPoint()->GetTouchable()
                                  ->GetVolume();

    if (pVol && pVol->GetLogicalVolume()->GetName() == "PhotAbsorberLV" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,1);
    }

}