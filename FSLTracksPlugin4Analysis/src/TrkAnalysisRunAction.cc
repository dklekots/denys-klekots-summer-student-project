#include "TrkAnalysisRunAction.hh"
#include "G4AnalysisManager.hh"
#include "G4UnitsTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4ThreadLocal std::vector<G4float> TrkAnalysisRunAction::fStepPointsX;
G4ThreadLocal std::vector<G4float> TrkAnalysisRunAction::fStepPointsY;
G4ThreadLocal std::vector<G4float> TrkAnalysisRunAction::fStepPointsZ;
G4ThreadLocal std::vector<G4float> TrkAnalysisRunAction::fStepPointsTime;

G4ThreadLocal G4int TrkAnalysisRunAction::trkNtupleID = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrkAnalysisRunAction::TrkAnalysisRunAction()
{
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    analysisManager->SetDefaultFileType("root");
    analysisManager->SetVerboseLevel(1);
    analysisManager->SetNtupleMerging(true);   
    analysisManager->SetFileName("Tracks_Information");

    trkNtupleID = analysisManager->CreateNtuple("Tracks", "Tracks information");
    
    analysisManager->CreateNtupleIColumn("particlePDG");            // columnID = 0
    analysisManager->CreateNtupleIColumn("isReachEndLightGuide");   // columnID = 1
    analysisManager->CreateNtupleIColumn("eventID");                // columnID = 2
    analysisManager->CreateNtupleFColumn("phi");                    // columnID = 3
    analysisManager->CreateNtupleFColumn("theta");                  // columnID = 4
    analysisManager->CreateNtupleFColumn("trkLength");              // columnID = 5
    analysisManager->CreateNtupleFColumn("trkTime");                // columnID = 6
    analysisManager->CreateNtupleFColumn("pointsX", fStepPointsX);
    analysisManager->CreateNtupleFColumn("pointsY", fStepPointsY);
    analysisManager->CreateNtupleFColumn("pointsZ", fStepPointsZ);
    analysisManager->CreateNtupleFColumn("pointsTime", fStepPointsTime);
    analysisManager->FinishNtuple(trkNtupleID);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrkAnalysisRunAction::~TrkAnalysisRunAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TrkAnalysisRunAction::BeginOfRunAction(const G4Run* /*aRun*/)
{
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    analysisManager->OpenFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TrkAnalysisRunAction::EndOfRunAction(const G4Run* /*aRun*/)
{
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    analysisManager->Write();
    analysisManager->CloseFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TrkAnalysisRunAction::ClearStepPoints()
{
    fStepPointsX.clear();
    fStepPointsY.clear();
    fStepPointsZ.clear();
    fStepPointsTime.clear();
}