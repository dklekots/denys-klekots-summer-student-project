#include "TrkAnalysisTrackingAction.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4Track.hh"
#include "G4AnalysisManager.hh"
#include "TrkAnalysisRunAction.hh"
#include "G4RunManager.hh"

TrkAnalysisTrackingAction::TrkAnalysisTrackingAction():G4UserTrackingAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrkAnalysisTrackingAction::~TrkAnalysisTrackingAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TrkAnalysisTrackingAction::PreUserTrackingAction(const G4Track* aTrack)
{
    G4int particlePdG = aTrack->GetParticleDefinition()->GetPDGEncoding();

    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,0,particlePdG);
    analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,0);

    TrkAnalysisRunAction::SetStepPointX(aTrack->GetPosition().getX());
    TrkAnalysisRunAction::SetStepPointY(aTrack->GetPosition().getY());
    TrkAnalysisRunAction::SetStepPointZ(aTrack->GetPosition().getZ());
    TrkAnalysisRunAction::SetStepPointTime(aTrack->GetGlobalTime());


    G4ThreeVector momDiection = aTrack->GetMomentumDirection();
    G4float theta = acos(momDiection.getZ());
    G4float phi;

    //Calculation of phi angle
    G4float momDirX = momDiection.getX();
    G4float momDirY = momDiection.getY();
    
    if(momDirX == 0)
    {
        if(momDirY > 0)
        {
            phi = 90 *deg;
        }
        else
        {
            phi = 270 *deg;
        }
    }
    else if (momDirX >= 0)
    {
        phi = atan(momDirY/momDirX);
    }
    else
    {
        phi = atan(momDirY/momDirX) + 180*deg;
    }

    if(phi < 0) phi+= 360*deg;

    //Fill angles into Ntuple
    analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID, 3, phi/deg);
    analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID, 4, theta/deg);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TrkAnalysisTrackingAction::PostUserTrackingAction(const G4Track* aTrack)
{
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

    G4int eventNum = G4RunManager::GetRunManager()->GetCurrentEvent()->
                                                           GetEventID();
    analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,2, eventNum);

    G4float trkLength = aTrack->GetTrackLength();
    G4float trkTime   = aTrack->GetLocalTime();

    analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID, 5, trkLength);
    analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID, 6, trkTime);

    analysisManager->AddNtupleRow(TrkAnalysisRunAction::trkNtupleID);
    

    TrkAnalysisRunAction::ClearStepPoints();
}