#ifndef TRKANALYSISSTEPPINGACTION_HH
#define TRKANALYSISSTEPPINGACTION_HH


#include "G4UserSteppingAction.hh"
#include "G4Step.hh"

class G4Step;

class TrkAnalysisSteppingAction: public G4UserSteppingAction
{
public:
    TrkAnalysisSteppingAction();
    ~TrkAnalysisSteppingAction();

    void UserSteppingAction(const G4Step* /*aStep*/);

};

#endif // TRKANALYSISSTEPPINGACTION_HH