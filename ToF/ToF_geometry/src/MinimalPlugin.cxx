#include "GeoModelKernel/GeoVGeometryPlugin.h"

// Geomodel includes
#include "GeoMaterial2G4/GeoExtendedMaterial.h"
#include "GeoModelKernel/GeoMaterial.h"
#include "GeoModelKernel/GeoBox.h"
#include "GeoModelKernel/GeoPara.h"
#include "GeoModelKernel/GeoShapeShift.h"
#include "GeoModelKernel/GeoShapeUnion.h"
#include "GeoModelKernel/GeoLogVol.h"
#include "GeoModelKernel/GeoPhysVol.h"
#include "GeoModelKernel/GeoTrap.h"
#include "GeoModelKernel/GeoTransform.h"

#include "GeoModelDBManager/GMDBManager.h"
#include "GeoModelWrite/WriteGeoModel.h"

// #include "GeoMaterial2G4/GeoMaterialPropertiesTable.h"

//  C++ includes
#include <string>
#include <fstream>
#include <math.h>

// Units
#include "GeoModelKernel/Units.h"
#define SYSTEM_OF_UNITS GeoModelKernelUnits // so we will get, e.g., 'GeoModelKernelUnits::cm'

// Define the units
#define g SYSTEM_OF_UNITS::gram
#define mole SYSTEM_OF_UNITS::mole
#define cm3 SYSTEM_OF_UNITS::cm3
#define cm SYSTEM_OF_UNITS::cm
#define mm SYSTEM_OF_UNITS::mm
#define deg SYSTEM_OF_UNITS::degree
#define rad SYSTEM_OF_UNITS::radian
#define eV SYSTEM_OF_UNITS::electronvolt

// Class Declaration

class MinimalPlugin : public GeoVGeometryPlugin
{

public:
    // Constructor:
    MinimalPlugin();

    // Destructor:
    ~MinimalPlugin();

    // Creation of geometry:
    virtual void create(GeoPhysVol *world, bool publish = false);

private:
    // Illegal operations:
    const MinimalPlugin &operator=(const MinimalPlugin &right) = delete;
    MinimalPlugin(const MinimalPlugin &right) = delete;
};

// Class definition:

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// Constructor
MinimalPlugin::MinimalPlugin()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// Destructor
MinimalPlugin::~MinimalPlugin()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

struct MaterialsStruct
{
    GeoExtendedMaterial *quartz = new GeoExtendedMaterial("Quartz", 2.203 * g / cm3);
    GeoExtendedMaterial *vacuum = new GeoExtendedMaterial("Vacuum", 1E-8 * g / cm3);
    GeoExtendedMaterial *pmtAbsorber = new GeoExtendedMaterial("pmtAbsorber", 2.203 * g / cm3);
    GeoExtendedMaterial *mirrorMaterial = new GeoExtendedMaterial("mirrorMaterial", 2.203 * g / cm3);
    
    GeoMaterial *siliconMaterial = new GeoMaterial("Silicon Material", 2.33 * g/cm3 );
}materials;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DefineMaterials()
{
    //-----------------------------------------------------------------------------------//
    // Define the materials that we shall use.                                           //
    // ----------------------------------------------------------------------------------//

    GeoElement *silicon = new GeoElement("Silicon", "Si", 14, 28.0855 * g / mole);
    GeoElement *oxygen = new GeoElement("Oxygen", "O", 8, 15.9994 * g / mole);

    materials.quartz->add(silicon, 28.0855);
    materials.quartz->add(oxygen, 2 * 15.9994);
    materials.quartz->lock();

    // materials.vacuum = new GeoExtendedMaterial("Vacuum", 1E-8 * g / cm3);
    materials.vacuum->add(oxygen, 1);
    materials.vacuum->lock();

    //any material to define absorber of photons at the end on light guide
    materials.pmtAbsorber->add(silicon, 28.0855);
    materials.pmtAbsorber->add(oxygen, 2 * 15.9994);
    materials.pmtAbsorber->lock();

    //any material to define absorber of photons at the end on light guide
    materials.mirrorMaterial->add(silicon, 28.0855);
    materials.mirrorMaterial->add(oxygen, 2 * 15.9994);
    materials.mirrorMaterial->lock();

    materials.siliconMaterial->add(silicon, 1);
    materials.siliconMaterial->lock();

    // define quartz material property
    double quartz_Energy[] = {7.0 * eV, 7.07 * eV, 7.14 * eV};
    double quartz_RIND[] = {1.59, 1.57, 1.54};
    double quartz_AbsLength[] = {420. * cm, 420. * cm, 420. * cm};
    GeoMaterialPropertiesTable *quartz_mt = new GeoMaterialPropertiesTable();
    quartz_mt->AddProperty("RINDEX", quartz_Energy, quartz_RIND, 3);
    quartz_mt->AddProperty("ABSLENGTH", quartz_Energy, quartz_AbsLength, 3);
    quartz_mt->AddConstProperty("RESOLUTIONSCALE", 0.1);
    materials.quartz->SetMaterialPropertiesTable(quartz_mt);

    // defiene vacuum material property
    double vacuum_Energy[] = {7.0 * eV, 7.07 * eV, 7.14 * eV};
    double vacuum_AbsLength[] = {1E10*cm, 1E10*cm, 1E10*cm};
    double vacuum_RIND[] = {1, 1, 1};
    GeoMaterialPropertiesTable *vacuum_mt = new GeoMaterialPropertiesTable();
    vacuum_mt->AddProperty("RINDEX", vacuum_Energy, vacuum_RIND, 3);
    vacuum_mt->AddProperty("ABSLENGTH", vacuum_Energy, vacuum_AbsLength, 3);
    vacuum_mt->AddConstProperty("RESOLUTIONSCALE", 0.1);
    materials.vacuum->SetMaterialPropertiesTable(vacuum_mt);

    // defiene mirror material property
    double mirror_Energy[] = {7.0 * eV, 7.07 * eV, 7.14 * eV};
    double mirror_AbsLength[] = {1E-9 * cm, 1E-9 * cm, 1E-9 * cm};
    double mirror_RIND[] = {1E10, 1E10, 1E10};
    GeoMaterialPropertiesTable *mirror_mt = new GeoMaterialPropertiesTable();
    mirror_mt->AddProperty("RINDEX", mirror_Energy, mirror_RIND, 3);
    mirror_mt->AddProperty("ABSLENGTH", mirror_Energy, mirror_AbsLength, 3);
    mirror_mt->AddConstProperty("RESOLUTIONSCALE", 0.1);
    materials.mirrorMaterial->SetMaterialPropertiesTable(mirror_mt);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

struct OrdinaryBarDimensions
{
    double yHalsSize;

    double ligGuiHalfThickness;
    double radHalfThickness;

    double ligGuiHalfLongLength;
    double ligGuiHalfShortLength;

    double radHalfLongLength;
    double radHalfShortLength;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void AddOrdinaryBar(GeoPhysVol *mother, OrdinaryBarDimensions dimensions, GeoTransform *plasePointShifting, std::string name)
{
    double yHalsSize = dimensions.yHalsSize;
    double ligGuiHalfThickness = dimensions.ligGuiHalfThickness;
    double radHalfThickness = dimensions.radHalfThickness;

    double ligGuiHalfLongLength = dimensions.ligGuiHalfLongLength;
    double ligGuiHalfShortLength = dimensions.ligGuiHalfShortLength;

    double radHalfLongLength = dimensions.radHalfLongLength;
    double radHalfShortLength = dimensions.radHalfShortLength;


    // creatr Radiator shape
    double ZHalfLength = yHalsSize;
    double Theta = atan((radHalfLongLength-radHalfShortLength)/2/yHalsSize)*rad;
    double Phi = 270 * deg; 
    double Dydzn = radHalfLongLength; 
    double Dxdyndzn = radHalfThickness; 
    double Dxdypdzn = radHalfThickness; 
    double Angleydzn = 0; 
    double Dydzp = radHalfShortLength; 
    double Dxdyndzp = radHalfThickness; 
    double Dxdypdzp = radHalfThickness;
    double Angleydzp = 0;
    const GeoTrap *sRadiator = new GeoTrap(ZHalfLength, Theta, Phi, Dydzn, Dxdyndzn, Dxdypdzn, Angleydzn,
                                           Dydzp, Dxdyndzp, Dxdypdzp, Angleydzp);

    // create light guide shape
    ZHalfLength = ligGuiHalfThickness;
    Theta = atan((ligGuiHalfLongLength-ligGuiHalfShortLength)/2/ligGuiHalfThickness)*rad;
    Phi = 270 * deg; 
    Dydzn = ligGuiHalfLongLength; 
    Dxdyndzn = yHalsSize; 
    Dxdypdzn = yHalsSize; 
    Angleydzn = 0; 
    Dydzp = ligGuiHalfShortLength; 
    Dxdyndzp = yHalsSize; 
    Dxdypdzp = yHalsSize;
    Angleydzp = 0;
    const GeoTrap *sLightGuide = new GeoTrap(ZHalfLength, Theta, Phi, Dydzn, Dxdyndzn, Dxdypdzn, Angleydzn,
                                           Dydzp, Dxdyndzp, Dxdypdzp, Angleydzp);

    // create transformation of light guide before shape union
    GeoTrf::RotateY3D shapeLightGuideRotateY(-90 * deg);
    GeoTrf::RotateZ3D shapeLightGuideRotateZ(90 * deg);
    double x = (ligGuiHalfShortLength+ligGuiHalfLongLength)/2;
    double y = -(radHalfShortLength+radHalfLongLength)/2-ligGuiHalfThickness;
    GeoTrf::Translate3D shapeLightGuideTranslate3D(x, y + 1E-5 * mm, 0);
    // The correction in y coordinate for program, to not insert world bolume between
    // light guide and radiator

    // chreate shifted light guide and apply all transformation
    //(in classes inplemented autodelete)
    GeoShapeShift *lightGuideShift = new GeoShapeShift(sLightGuide, shapeLightGuideRotateY);
    lightGuideShift = new GeoShapeShift(lightGuideShift, shapeLightGuideRotateZ);
    lightGuideShift = new GeoShapeShift(lightGuideShift, shapeLightGuideTranslate3D);

    // create Train2BarALV volume
    const GeoShapeUnion *sTrain2BarA = new GeoShapeUnion(sRadiator, lightGuideShift);
    const GeoLogVol *lTrain2BarA = new GeoLogVol(name, sTrain2BarA, materials.quartz);
    GeoPhysVol *pTrain2BarA = new GeoPhysVol(lTrain2BarA);

    //create photon absorber at the end of light guide and add it to the pTrain2BarA
    const GeoBox *sPhotAbsorber = new GeoBox(1E-5*mm, ligGuiHalfThickness, yHalsSize);
    const GeoLogVol *lPhotAbsorber = new GeoLogVol(name+"_PhotAbsorberLV", sPhotAbsorber, materials.pmtAbsorber);
    GeoPhysVol *pPhotAbsorber = new GeoPhysVol(lPhotAbsorber);
    x = ligGuiHalfLongLength+ligGuiHalfShortLength;
    y = -(radHalfShortLength+radHalfLongLength)/2-ligGuiHalfThickness;
    GeoTrf::Translate3D absorTranslate3D(x - 1E-5 * mm, y, 0);
    GeoTransform *absorPlacement = new GeoTransform(absorTranslate3D);
    pTrain2BarA->add(absorPlacement);
    pTrain2BarA->add(pPhotAbsorber);

    //create elbow mirror and add it to the pTrain2BarA
    double angle = -atan((ligGuiHalfLongLength-ligGuiHalfShortLength)/ligGuiHalfThickness)*rad;
    const GeoPara *sElbowMirror = new GeoPara(1E-5*mm, ligGuiHalfThickness, yHalsSize, angle, 0, 0);
    const GeoLogVol *lElbowMirror = new GeoLogVol(name+"_ElbowMirrorLV", sElbowMirror, materials.mirrorMaterial);
    GeoPhysVol *pElbowMirror = new GeoPhysVol(lElbowMirror);
    y = -(radHalfShortLength+radHalfLongLength)/2-ligGuiHalfThickness;
    GeoTrf::TranslateY3D mirrorTranslateY3D(y + 1E-5 * mm);
    GeoTransform *mirrorPlacement = new GeoTransform(mirrorTranslateY3D);
    pTrain2BarA->add(mirrorPlacement);
    pTrain2BarA->add(pElbowMirror);

    y = (radHalfShortLength+radHalfLongLength)/2 + (radHalfLongLength-radHalfShortLength);
    GeoTrf::Translate3D coorTranslate(radHalfThickness, y, yHalsSize);
    GeoTrf::RotateZ3D coorRotate(180 * deg);
    GeoTransform *cootTransorm1 = new GeoTransform(coorTranslate);
    GeoTransform *cootTransorm2 = new GeoTransform(coorRotate);
    mother->add(cootTransorm1);
    mother->add(cootTransorm2);
    mother->add(plasePointShifting);
    mother->add(pTrain2BarA);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

struct ThinBarDimensions
{
    double yHalsSize;

    double ordLigGuiHalfThickness;
    double additLigGuiHalfThickness;
    double radHalfThickness;

    double ordLigGuiHalfLongLength;
    double ordLigGuiHalfShortLength;

    double additLigGuiHalfLongLength;
    double additLigGuiHalfShortLength;

    double radHalfLongLength;
    double radHalfShortLength;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void AddThinBar(GeoPhysVol *mother, ThinBarDimensions dimensions, GeoTransform *plasePointShifting, std::string name)
{
    double yHalsSize = dimensions.yHalsSize;

    double ordLigGuiHalfThickness = dimensions.ordLigGuiHalfThickness;
    double additLigGuiHalfThickness = dimensions.additLigGuiHalfThickness;
    double radHalfThickness = dimensions.radHalfThickness;

    double ordLigGuiHalfLongLength = dimensions.ordLigGuiHalfLongLength;
    double ordLigGuiHalfShortLength = dimensions.ordLigGuiHalfShortLength;

    double additLigGuiHalfLongLength = dimensions.additLigGuiHalfLongLength;
    double additLigGuiHalfShortLength = dimensions.additLigGuiHalfShortLength;

    double radHalfLongLength = dimensions.radHalfLongLength;
    double radHalfShortLength = dimensions.radHalfShortLength;


    // creatr Radiator shape
    double ZHalfLength = yHalsSize;
    double Theta = atan((radHalfLongLength-radHalfShortLength)/2/yHalsSize)*rad;
    double Phi = 270 * deg; 
    double Dydzn = radHalfLongLength; 
    double Dxdyndzn = radHalfThickness; 
    double Dxdypdzn = radHalfThickness; 
    double Angleydzn = 0; 
    double Dydzp = radHalfShortLength; 
    double Dxdyndzp = radHalfThickness; 
    double Dxdypdzp = radHalfThickness;
    double Angleydzp = 0;
    const GeoTrap *sRadiator = new GeoTrap(ZHalfLength, Theta, Phi, Dydzn, Dxdyndzn, Dxdypdzn, Angleydzn,
                                           Dydzp, Dxdyndzp, Dxdypdzp, Angleydzp);

    // create ordinary light guide shape
    ZHalfLength = ordLigGuiHalfThickness;
    Theta = atan((ordLigGuiHalfLongLength-ordLigGuiHalfShortLength)/2/ordLigGuiHalfThickness)*rad;
    Phi = 270 * deg; 
    Dydzn = ordLigGuiHalfLongLength; 
    Dxdyndzn = yHalsSize; 
    Dxdypdzn = yHalsSize; 
    Angleydzn = 0; 
    Dydzp = ordLigGuiHalfShortLength; 
    Dxdyndzp = yHalsSize; 
    Dxdypdzp = yHalsSize;
    Angleydzp = 0;
    const GeoTrap *sOrdLightGuide = new GeoTrap(ZHalfLength, Theta, Phi, Dydzn, Dxdyndzn, Dxdypdzn, Angleydzn,
                                           Dydzp, Dxdyndzp, Dxdypdzp, Angleydzp);

    // create additional light guide shape
    ZHalfLength = additLigGuiHalfThickness;
    Theta = atan((additLigGuiHalfLongLength-additLigGuiHalfShortLength)/2/additLigGuiHalfThickness)*rad;
    Phi = 270 * deg; 
    Dydzn = additLigGuiHalfLongLength; 
    Dxdyndzn = yHalsSize; 
    Dxdypdzn = yHalsSize; 
    Angleydzn = 0; 
    Dydzp = additLigGuiHalfShortLength; 
    Dxdyndzp = yHalsSize; 
    Dxdypdzp = yHalsSize;
    Angleydzp = 0;
    const GeoTrap *sAdditLightGuide = new GeoTrap(ZHalfLength, Theta, Phi, Dydzn, Dxdyndzn, Dxdypdzn, Angleydzn,
                                           Dydzp, Dxdyndzp, Dxdypdzp, Angleydzp);

    //shift additional light guide and unite it with an ordinary one
    double y = -((ordLigGuiHalfShortLength+ordLigGuiHalfLongLength)/2 - (additLigGuiHalfShortLength+additLigGuiHalfLongLength)/2);
    double z = ordLigGuiHalfThickness+additLigGuiHalfThickness-1E-5;
    GeoTrf::Translate3D additLightGuiTranslate3D(0,y,z);
    GeoShapeShift *additLightGuideShift = new GeoShapeShift(sAdditLightGuide, additLightGuiTranslate3D);
    const GeoShapeUnion *sLightGuide = new GeoShapeUnion(additLightGuideShift, sOrdLightGuide);

    // create transformation of light guide before shape union
    GeoTrf::RotateY3D shapeLightGuideRotateY(-90 * deg);
    GeoTrf::RotateZ3D shapeLightGuideRotateZ(90 * deg);
    double x = (ordLigGuiHalfShortLength+ordLigGuiHalfLongLength)/2;
    y = -(radHalfShortLength+radHalfLongLength)/2-ordLigGuiHalfThickness;
    GeoTrf::Translate3D shapeLightGuideTranslate3D(x, y + 1E-5 * mm, 0);
    // The correction in y coordinate for program, to not insert world bolume between
    // light guide and radiator

    // chreate shifted light guide and apply all transformation
    //(in classes inplemented autodelete)
    GeoShapeShift *lightGuideShift = new GeoShapeShift(sLightGuide, shapeLightGuideRotateY);
    lightGuideShift = new GeoShapeShift(lightGuideShift, shapeLightGuideRotateZ);
    lightGuideShift = new GeoShapeShift(lightGuideShift, shapeLightGuideTranslate3D);

    // create Train2BarALV volume
    const GeoShapeUnion *sTrain2BarA = new GeoShapeUnion(sRadiator, lightGuideShift);
    const GeoLogVol *lTrain2BarA = new GeoLogVol(name, sTrain2BarA, materials.quartz);
    GeoPhysVol *pTrain2BarA = new GeoPhysVol(lTrain2BarA);

    //create photon absorber at the end of ordinary light guide and add it to the pTrain2BarA
    const GeoBox *sOrdPhotAbsorber = new GeoBox(1E-5*mm, ordLigGuiHalfThickness, yHalsSize);
    const GeoLogVol *lOrdPhotAbsorber = new GeoLogVol(name+"_PhotAbsorberLV", sOrdPhotAbsorber, materials.pmtAbsorber);
    GeoPhysVol *pOrdPhotAbsorber = new GeoPhysVol(lOrdPhotAbsorber);
    x = ordLigGuiHalfLongLength+ordLigGuiHalfShortLength;
    y = -(radHalfShortLength+radHalfLongLength)/2-ordLigGuiHalfThickness;
    GeoTrf::Translate3D ordAbsorTranslate3D(x - 1E-5 * mm, y, 0);
    GeoTransform *ordAbsorPlacement = new GeoTransform(ordAbsorTranslate3D);
    pTrain2BarA->add(ordAbsorPlacement);
    pTrain2BarA->add(pOrdPhotAbsorber);

    //create photon absorber at the end of additional light guide and add it to the pTrain2BarA
    const GeoBox *sAdditPhotAbsorber = new GeoBox(1E-5*mm, additLigGuiHalfThickness, yHalsSize);
    const GeoLogVol *lAdditPhotAbsorber = new GeoLogVol(name+"_PhotAbsorberLV", sAdditPhotAbsorber, materials.pmtAbsorber);
    GeoPhysVol *pAdditPhotAbsorber = new GeoPhysVol(lAdditPhotAbsorber);
    x = ordLigGuiHalfLongLength+ordLigGuiHalfShortLength;
    y = -(radHalfShortLength+radHalfLongLength)/2-ordLigGuiHalfThickness*2-additLigGuiHalfThickness;
    GeoTrf::Translate3D additAbsorTranslate3D(x - 1E-5 * mm, y, 0);
    GeoTransform *additAdditAbsorPlacement = new GeoTransform(additAbsorTranslate3D);
    pTrain2BarA->add(additAdditAbsorPlacement);
    pTrain2BarA->add(pAdditPhotAbsorber);

    //create ordinary elbow mirror and add it to the pTrain2BarA
    double angle = -atan((ordLigGuiHalfLongLength-ordLigGuiHalfShortLength)/ordLigGuiHalfThickness)*rad;
    const GeoPara *sOrdElbowMirror = new GeoPara(1E-5*mm, ordLigGuiHalfThickness, yHalsSize, angle, 0, 0);
    const GeoLogVol *lOrdElbowMirror = new GeoLogVol(name+"_ordElbowMirrorLV", sOrdElbowMirror, materials.mirrorMaterial);
    GeoPhysVol *pOrdElbowMirror = new GeoPhysVol(lOrdElbowMirror);
    y = -(radHalfShortLength+radHalfLongLength)/2-ordLigGuiHalfThickness;
    GeoTrf::TranslateY3D ordMirrorTranslateY3D(y + 1E-5 * mm);
    GeoTransform *ordMirrorPlacement = new GeoTransform(ordMirrorTranslateY3D);
    pTrain2BarA->add(ordMirrorPlacement);
    pTrain2BarA->add(pOrdElbowMirror);

    //create additional elbow mirror and add it to the pTrain2BarA
    angle = -atan((additLigGuiHalfLongLength-additLigGuiHalfShortLength)/additLigGuiHalfThickness)*rad;
    const GeoPara *sAdditElbowMirror = new GeoPara(1E-5*mm, additLigGuiHalfThickness, yHalsSize, angle, 0, 0);
    const GeoLogVol *lAdditElbowMirror = new GeoLogVol(name+"_additElbowMirrorLV", sAdditElbowMirror, materials.mirrorMaterial);
    GeoPhysVol *pAdditElbowMirror = new GeoPhysVol(lAdditElbowMirror);
    x = (ordLigGuiHalfLongLength-ordLigGuiHalfShortLength) + additLigGuiHalfLongLength-additLigGuiHalfShortLength;
    y = -(radHalfShortLength+radHalfLongLength)/2-ordLigGuiHalfThickness*2-additLigGuiHalfThickness;
    GeoTrf::Translate3D additMirrorTranslate3D(x, y + 2E-5 * mm, 0);
    GeoTransform *additMirrorPlacement = new GeoTransform(additMirrorTranslate3D);
    pTrain2BarA->add(additMirrorPlacement);
    pTrain2BarA->add(pAdditElbowMirror);

    y = (radHalfShortLength+radHalfLongLength)/2 + (radHalfLongLength-radHalfShortLength);
    GeoTrf::Translate3D coorTranslate(radHalfThickness, y, yHalsSize);
    GeoTrf::RotateZ3D coorRotate(180 * deg);
    GeoTransform *cootTransorm1 = new GeoTransform(coorTranslate);
    GeoTransform *cootTransorm2 = new GeoTransform(coorRotate);
    mother->add(cootTransorm1);
    mother->add(cootTransorm2);
    mother->add(plasePointShifting);
    mother->add(pTrain2BarA);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MinimalPlugin::create(GeoPhysVol *globalWorld, bool /*publish*/)
{
    DefineMaterials();

    // Create a world volume
    const GeoBox *sWorld = new GeoBox(100 * mm, 100 * mm, 100 * mm);
    const GeoLogVol *lWorld = new GeoLogVol("WorldLV", sWorld, materials.vacuum);
    GeoPhysVol *pWorld = new GeoPhysVol(lWorld);
    
    for (int barNum = 0; barNum < 4; ++barNum)
    {
        for (int trainNum = 0; trainNum < 3; ++trainNum)
        {
            OrdinaryBarDimensions barDims;
            barDims.yHalsSize = 3*mm;
            barDims.ligGuiHalfThickness = 2.5*mm;
            barDims.radHalfThickness = 2.5*mm;
            barDims.ligGuiHalfLongLength = (60.0+5*trainNum+0.1*trainNum)/2*mm;
            barDims.ligGuiHalfShortLength = (55.0+5*trainNum+0.1*trainNum)/2*mm;
            barDims.radHalfLongLength = (24.8+(5.4+0.25)*barNum+6.25*trainNum)/2*mm;
            barDims.radHalfShortLength = (19.4+(5.4+0.25)*barNum+6.25*trainNum)/2*mm;    
            GeoTrf::Translate3D tmpTranslate3D(-5*trainNum*mm-0.1*trainNum*mm, (5.4+0.25)*barNum*mm, (-6-0.25)*barNum*mm);
            GeoTransform *tmpTransform = new GeoTransform(tmpTranslate3D);
            AddOrdinaryBar(pWorld, barDims, tmpTransform, "Train"+std::to_string(1+trainNum)+
                                                                    "Bar"+char(68-barNum)+"LV");
        }
    }

    for (int barNum = 0; barNum < 4; ++barNum)
    {
        int trainNum = 3;

        ThinBarDimensions barDims;
        barDims.yHalsSize = 3*mm;
        barDims.ordLigGuiHalfThickness = 1.5*mm;
        barDims.additLigGuiHalfThickness = 1*mm;
        barDims.radHalfThickness = 1.5*mm;
        barDims.ordLigGuiHalfLongLength = (70+3+0.1*3)/2.0*mm;
        barDims.ordLigGuiHalfShortLength = (70+0.1*3)/2.0*mm;
        barDims.additLigGuiHalfLongLength = 70.3/2.0*mm;
        barDims.additLigGuiHalfShortLength = 64.14/2.0*mm;
        barDims.radHalfLongLength = (24.8+(5.4+0.25)*barNum+6.25*trainNum)/2.0*mm;
        barDims.radHalfShortLength = (19.4+(5.4+0.25)*barNum+6.25*trainNum)/2.0*mm;    
        GeoTrf::Translate3D tmpTranslate3D(-5*trainNum*mm-0.1*3*mm, (5.4+0.25)*barNum*mm, (-6-0.25)*barNum*mm);
        GeoTransform *tmpTransform = new GeoTransform(tmpTranslate3D);
        AddThinBar(pWorld, barDims, tmpTransform, "Train"+std::to_string(1+trainNum)+
                                                                "Bar"+char(68-barNum)+"LV");
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    //Silicon Trackers
    ///////////////////////////////////////////////////////////////////////////////////////////////////

    const GeoBox *sSiTracker = new GeoBox( 9.6*mm, 10.25*mm, 0.5*mm);
    const GeoLogVol* lSiTracker = new GeoLogVol("Silicon Tracker", sSiTracker, materials.siliconMaterial);
    GeoPhysVol* pSiTracker = new GeoPhysVol(lSiTracker);

    GeoTrf::RotateX3D siTrCoorRotation(-42*deg);
    GeoTrf::Translate3D siTrTranslation(9.18*mm, 10.25*mm, -39.18*mm);
    GeoTrf::TranslateZ3D siTrDeltaTranslation(-9.00*mm);
    GeoTrf::RotateY3D siOwnrotation(14*deg);

    GeoTransform *siTrCoorRotationTransform = new GeoTransform(siTrCoorRotation);
    GeoTransform *siTrTranslationTransform = new GeoTransform(siTrTranslation);
    GeoTransform *siTrDeltaTranslationTransform = new GeoTransform(siTrDeltaTranslation);
    GeoTransform *siOwnrotationTransform = new GeoTransform(siOwnrotation);

    // pWorld->add(siTrCoorRotationTransform);
    // pWorld->add(siTrTranslationTransform);
    // pWorld->add(siOwnrotationTransform);
    // pWorld->add(pSiTracker);


    for (int i=0; i<4; i++)
    {
        pWorld->add(siTrCoorRotationTransform);
        pWorld->add(siTrTranslationTransform);

        for(int j=0; j<i; ++j)
            pWorld->add(siTrDeltaTranslationTransform);

        pWorld->add(siOwnrotationTransform);
        pWorld->add(pSiTracker);
    }


    globalWorld->add(pWorld);
}

// The name of this routine must correspond to the name of the class,
// and also to the name of the source code file (this file)

extern "C" MinimalPlugin *createMinimalPlugin()
{
    return new MinimalPlugin;
}
