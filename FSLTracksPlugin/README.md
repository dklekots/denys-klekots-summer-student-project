The fullSimLight plugin, for generating Tracks_data.h5 which works as an input file for track visualisation in gmex.

Run in terminal the following commands for building plugin
```
mkdir build
cd build
cmake ../
make -j
```
