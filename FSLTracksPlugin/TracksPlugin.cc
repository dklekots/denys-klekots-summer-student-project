#include "FullSimLight/FSLUserActionPlugin.h"
#include <iostream>

#include "TrksRunAction.hh"
#include "TrksSteppingAction.hh"
#include "TrksTrackingAction.hh"
#include "TrksEventAction.hh"

class TracksPlugin:public FSLUserActionPlugin {
    
public:
    
    TracksPlugin();

    G4UserRunAction* getRunAction() const;
    G4UserEventAction* getEventAction() const;
    G4UserTrackingAction* getTrackingAction() const;
    G4UserSteppingAction* getSteppingAction() const;
};

TracksPlugin::TracksPlugin()
{

}

G4UserRunAction* TracksPlugin::getRunAction() const
{
    return new TrksRunAction;
}

G4UserEventAction* TracksPlugin::getEventAction() const
{
    return new TrksEventAction;
}

G4UserTrackingAction* TracksPlugin::getTrackingAction() const
{
    return new TrksTrackingAction;
}


G4UserSteppingAction* TracksPlugin::getSteppingAction() const
{
    return new TrksSteppingAction;
}

extern "C" TracksPlugin *createTracksPlugin() {
    return new TracksPlugin();
}

