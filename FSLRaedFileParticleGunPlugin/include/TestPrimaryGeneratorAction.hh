// #ifndef _G01PRIMARYGENERATORACTION_H_
// #define _G01PRIMARYGENERATORACTION_H_

#ifndef _TESTPRIMARYGENERATORACTION_hh_
#define _TESTPRIMARYGENERATORACTION_hh_

#include "G4VUserPrimaryGeneratorAction.hh"

#include "G4ParticleGun.hh"

#include "globals.hh"

#include <fstream>
#include <mutex>

class G4Event;
// class G4GeneralParticleSource;
class G4ParticleGun;

/// Minimal primary generator action to demonstrate the use of GDML geometries

class TestPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:

  TestPrimaryGeneratorAction();
  ~TestPrimaryGeneratorAction();

  virtual void GeneratePrimaries(G4Event* anEvent);

private:

  // G4GeneralParticleSource* fParticleSource;
  G4ParticleGun* fParticleGun;

  static std::mutex fgFileReadMutex;

  static std::ifstream *fgInputFile;

  static G4int fgNInstance;

  G4double fRefMomentumValue;
};

#endif
