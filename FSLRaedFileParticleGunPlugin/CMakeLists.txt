# (C) 2002-2022 CERN for the benefit of the ATLAS collaboration

# Set up the project.
cmake_minimum_required( VERSION 3.1 )
project( "FSLTestParticleGunPlugin" )

# I am built as a top-level project.
# Make the root module directory visible to CMake.
list( APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR} )
# get global GeoModel version
include( GeoModelATLAS-version ) 
# set the project, with the version taken from the GeoModel parent project
project( "FSLTestParticleGunPlugin" VERSION ${GeoModelATLAS_VERSION} LANGUAGES CXX )
# Define color codes for CMake messages
include( cmake_colors_defs )
# Warn the users about what they are doing
message(STATUS "${BoldGreen}Building ${PROJECT_NAME} individually, as a top-level project.${ColourReset}")
# Set default build and C++ options
include( configure_cpp_options )
set( CMAKE_FIND_FRAMEWORK "LAST" CACHE STRING
   "Framework finding behaviour on macOS" )
# Set up how the project handle some of its dependencies. Either by picking them
# up from the environment, or building them itself.


# Project's dependencies.
# GeoModel dependencies
find_package( FullSimLight REQUIRED )

find_package( Geant4 REQUIRED )
message( STATUS "Found Geant4: ${Geant4_INCLUDE_DIR}")
#message("Geant4_USE_FILE: ${Geant4_USE_FILE}") # debug msg
include(${Geant4_USE_FILE})

# Use the GNU install directory names.
include( GNUInstallDirs )

# Find the header and source files.
file( GLOB HEADERS ${PROJECT_SOURCE_DIR}/include/*.hh)
file( GLOB SOURCES ${PROJECT_SOURCE_DIR}/src/*.cc )

# Set up the library.
add_library( TestActionsPrimaryGeneratorActionPlugin SHARED 
TestActionsPrimaryGeneratorActionPlugin.cc ${SOURCES} ${HEADERS})

target_link_libraries ( TestActionsPrimaryGeneratorActionPlugin PUBLIC FullSimLight ${CMAKE_DL_LIBS} ${Geant4_LIBRARIES} )
target_include_directories( TestActionsPrimaryGeneratorActionPlugin PUBLIC 
   $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
   $<INSTALL_INTERFACE:include> ${FullSimLight_INCLUDE_DIR} )


source_group( "src" FILES ${SOURCES} )
set_target_properties( TestActionsPrimaryGeneratorActionPlugin PROPERTIES
   VERSION ${PROJECT_VERSION}
   SOVERSION ${PROJECT_VERSION_MAJOR} )

# Install the library.
install( TARGETS TestActionsPrimaryGeneratorActionPlugin
   EXPORT ${PROJECT_NAME}-export
   LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
   COMPONENT Runtime
   NAMELINK_SKIP )
