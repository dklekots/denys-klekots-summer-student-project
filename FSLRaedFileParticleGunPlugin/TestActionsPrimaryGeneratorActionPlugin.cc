#include "FullSimLight/FSLUserActionPlugin.h"
#include <iostream>

#include "TestPrimaryGeneratorAction.hh"

class MyUserActionsPlugin:public FSLUserActionPlugin {
    
public:
    
    MyUserActionsPlugin();
    // virtual const G4VUserActionInitialization *getUserActionInitialization() const final;

    G4VUserPrimaryGeneratorAction *getPrimaryGeneratorAction() const;
};

MyUserActionsPlugin::MyUserActionsPlugin()
{
    std::cout<<"Hello from the MyUserGunActionsPlugin"<<std::endl;
}

G4VUserPrimaryGeneratorAction* MyUserActionsPlugin::getPrimaryGeneratorAction() const
{
    return new TestPrimaryGeneratorAction;
}

extern "C" MyUserActionsPlugin *createTestActionsPrimaryGeneratorActionPlugin() {
    return new MyUserActionsPlugin();
}

