#include "TestPrimaryGeneratorAction.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4SystemOfUnits.hh"

std::mutex TestPrimaryGeneratorAction::fgFileReadMutex;
std::ifstream* TestPrimaryGeneratorAction::fgInputFile = nullptr;
G4int TestPrimaryGeneratorAction::fgNInstance=0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestPrimaryGeneratorAction::TestPrimaryGeneratorAction()
 : G4VUserPrimaryGeneratorAction(),
  fParticleGun(0)
{

  G4int n_particle = 1;
  fParticleGun = new G4ParticleGun(n_particle);

  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  fParticleGun->SetParticleDefinition(
               particleTable->FindParticle(particleName="proton"));

  fgFileReadMutex.lock();
  if (fgInputFile == nullptr)
  {
    fgInputFile = new std::ifstream("ParticleGunInputData.txt");
  }
  fgNInstance++;
  fgFileReadMutex.unlock();

  fRefMomentumValue = 7*TeV;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestPrimaryGeneratorAction::~TestPrimaryGeneratorAction()
{
  delete fParticleGun;
    
  fgFileReadMutex.lock();
  fgNInstance--;
  if(fgNInstance == 0)
  {
    delete fgInputFile;
    fgInputFile = nullptr;
  }
  fgFileReadMutex.unlock();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TestPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  G4double inX;
  G4double inY;
  G4double inZ;
  G4double inThetax;
  G4double inThetay;
  G4double deltaP;
  G4double outX;
  G4double outY;
  G4double outZ;
  G4double outThetax;
  G4double outThetay;
  G4double isDetected;

  fgFileReadMutex.lock();
  
  std::ifstream &file = *fgInputFile;

  file>>inX;
  file>>inY;
  file>>inZ;
  file>>inThetax;
  file>>inThetay;
  file>>deltaP;
  file>>outX;
  file>>outY;
  file>>outZ;
  file>>outThetax;
  file>>outThetay;
  file>>isDetected;

  inX = inX*m;
  inY = inY*m;
  inZ = inZ*m;
  inThetax = inThetax*mrad;
  inThetay = inThetay*mrad;
  deltaP = deltaP*1;
  outX = outX*m;
  outY = outY*m;
  outZ = outZ*m;
  outThetax = outThetax*mrad;
  outThetay = outThetay*mrad;
  isDetected = isDetected*1;

  fParticleGun->SetParticleMomentum(fRefMomentumValue*(1+deltaP));
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector( outThetax, outThetay, 1));
  fParticleGun->SetParticlePosition(G4ThreeVector(outX, outY, outZ));

  fParticleGun->GeneratePrimaryVertex(anEvent);

  
  if(fgInputFile->eof())  //if reached the end of the file
  {
    G4cerr << "The end of the input file reached! Aborting" << G4endl;
    exit(-1);
  }
  fgFileReadMutex.unlock();

}
