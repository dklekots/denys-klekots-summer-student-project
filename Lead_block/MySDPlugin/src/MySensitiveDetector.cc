#include "MySensitiveDetector.hh"

#include "G4Step.hh"
#include "G4AnalysisManager.hh"
#include "G4RunManager.hh"

class G4Step;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

MySensitiveDetector::MySensitiveDetector() : 
                            G4VSensitiveDetector("MySensitiveDetector")
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

MySensitiveDetector::~MySensitiveDetector()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool MySensitiveDetector::ProcessHits(G4Step * aStep, G4TouchableHistory* ) 
{
    G4Track* aTrack = aStep->GetTrack();

    G4int eventNum = G4RunManager::GetRunManager()->GetCurrentEvent()->
                                                    GetEventID();
    G4int trkID = aTrack->GetTrackID();
    G4int particlePDG = aTrack->GetParticleDefinition()->GetPDGEncoding();
    G4ThreeVector preStepPoint = aStep->GetPreStepPoint()->GetPosition();
    G4ThreeVector postStepPoint = aStep->GetPostStepPoint()->GetPosition();
    G4float eDep = aStep->GetTotalEnergyDeposit();
    G4float EkinPre = aStep->GetPreStepPoint()->GetKineticEnergy();
    G4float EkinPost = aStep->GetPostStepPoint()->GetKineticEnergy();
    G4float trkLen = aStep->GetTrack()->GetTrackLength();

    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

    analysisManager->FillNtupleIColumn(0, eventNum);
    analysisManager->FillNtupleIColumn(1, trkID);
    analysisManager->FillNtupleIColumn(2, particlePDG);
    analysisManager->FillNtupleFColumn(3, preStepPoint.x());
    analysisManager->FillNtupleFColumn(4, preStepPoint.y());
    analysisManager->FillNtupleFColumn(5, preStepPoint.z());
    analysisManager->FillNtupleFColumn(6, postStepPoint.x());
    analysisManager->FillNtupleFColumn(7, postStepPoint.y());
    analysisManager->FillNtupleFColumn(8, postStepPoint.z());
    analysisManager->FillNtupleFColumn(9, eDep);
    analysisManager->FillNtupleFColumn(10, EkinPre);
    analysisManager->FillNtupleFColumn(11, EkinPost);
    analysisManager->FillNtupleFColumn(12, trkLen);
    analysisManager->AddNtupleRow();

    return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MySensitiveDetector::Initialize(G4HCofThisEvent *)
{
    // G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    // analysisManager->OpenFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MySensitiveDetector::EndOfEvent(G4HCofThisEvent *)
{
    // G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    // analysisManager->Write();
    // analysisManager->CloseFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

