#include "FullSimLight/FSLSensitiveDetectorPlugin.h"

#include "MySensitiveDetector.hh"

#include "G4AnalysisManager.hh"


#include <iostream>



class MySDPlugin:public FSLSensitiveDetectorPlugin {

public:
  
  MySDPlugin();
  ~MySDPlugin();
  
  virtual G4VSensitiveDetector *getSensitiveDetector() const final override;

  virtual std::string getHitCollectionName() const final override; 


};

MySDPlugin::MySDPlugin()
{
  
  // Specify the volumes to which we attach the sensitive detector:
  addLogicalVolumeName("SliceSencLV");

  // G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  
  // analysisManager->SetFileName("MySDOutput.root");  
  // analysisManager->SetNtupleMerging(true);

  // analysisManager->CreateNtuple("MyOutput", "Steps"); // NtupleID = 0

  // analysisManager->CreateNtupleIColumn("eventID");    // columnID = 0
  // analysisManager->CreateNtupleIColumn("trkID");      // columnID = 1
  // analysisManager->CreateNtupleIColumn("particlePDG");// columnID = 2
  // analysisManager->CreateNtupleFColumn("preX");       // columnID = 3
  // analysisManager->CreateNtupleFColumn("preY");       // columnID = 4
  // analysisManager->CreateNtupleFColumn("preZ");       // columnID = 5
  // analysisManager->CreateNtupleFColumn("postX");      // columnID = 6
  // analysisManager->CreateNtupleFColumn("postY");      // columnID = 7
  // analysisManager->CreateNtupleFColumn("postZ");      // columnID = 8
  // analysisManager->CreateNtupleFColumn("eDep");       // columnID = 9
  // analysisManager->CreateNtupleFColumn("EkinPre");    // columnID = 10
  // analysisManager->CreateNtupleFColumn("EkinPost");   // columnID = 11
  // analysisManager->CreateNtupleFColumn("trkLen");     // cplumnID = 12
  // analysisManager->FinishNtuple(0);

}

MySDPlugin::~MySDPlugin()
{
  std::cout << "GOODBYE from BeampipeSDPlugin" << std::endl;
}

std::string MySDPlugin::getHitCollectionName() const 
{
  return "MyHitCollection";
}

G4VSensitiveDetector *MySDPlugin::getSensitiveDetector() const 
{
  return new MySensitiveDetector();
}

extern "C" MySDPlugin *createMySDPlugin() 
{
  return new MySDPlugin();
}

