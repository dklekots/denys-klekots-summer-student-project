#include "G4VSensitiveDetector.hh"

#include <iostream>

class MySensitiveDetector:public G4VSensitiveDetector {
public:

  // Constructor:
  MySensitiveDetector();

  // Destructor:
  ~MySensitiveDetector();

  // Process hits
  virtual G4bool ProcessHits(G4Step*, G4TouchableHistory*)  override  final;

  virtual void Initialize(G4HCofThisEvent *) override;

  virtual void EndOfEvent(G4HCofThisEvent *) override;

private:

};