#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"
#include "vector"
#include "TBranch.h"
#include "TH2F.h"
#include "iostream"
#include "TCanvas.h"

using namespace::std;

vector<vector<double>> getMesh(int hBinsN, int vBinsN, Float_t xMin, Float_t xMax,
                         Float_t yMin, Float_t yMax, TTree* tree)
{
    Float_t PrexCoor;
    Float_t PreyCoor;
    Float_t PostxCoor;
    Float_t PostyCoor;
    Float_t eDep;

    vector<vector<double>> mesh(hBinsN, vector<double>(300));
    // for(int i=0; i<hBinsN; ++i)
    // {
    //     mesh[i].reserve(vBinsN);
    //     cout << "Ok" << endl;
    // }
    
    Double_t hStep = (xMax-xMin)/hBinsN;
    Double_t vStep = (yMax-yMin)/vBinsN;

    TBranch  *PrexCoorBr = tree->GetBranch("preX");
    TBranch  *PreyCoorBr = tree->GetBranch("preY");
    TBranch  *PostxCoorBr = tree->GetBranch("postX");
    TBranch  *PostyCoorBr = tree->GetBranch("postY");
    TBranch  *eDepBr = tree->GetBranch("eDep");

    PrexCoorBr ->SetAddress(&PrexCoor);
    PreyCoorBr ->SetAddress(&PreyCoor);
    PostxCoorBr->SetAddress(&PostxCoor);
    PostyCoorBr->SetAddress(&PostyCoor);
    eDepBr->SetAddress(&eDep); 

    Int_t entryN = tree->GetEntries();

    for (Int_t entry_i=0; entry_i<entryN; ++entry_i)
    {
        tree->GetEntry(entry_i);

        Int_t i = ((PrexCoor+PostxCoor)/2-xMin)/hStep;
        Int_t j = ((PreyCoor+PostyCoor)/2-yMin)/vStep;

        if (i >= 0 && i < hBinsN && j >= 0 && j < vBinsN)
        {
            mesh[i][j]+=eDep;
        }

    }

    return mesh;
}

void BuildPlot_eDep_mesh()
{
    TFile *file = new TFile("../MyUserActionOutput.root");
    TTree *tree = (TTree*)file->Get("MyOutput;1");

    // gStyle->SetOptStat(2);

    Int_t hBinsN = 300; 
    Int_t vBinsN = 300;
    Float_t xMin = -3000;
    Float_t xMax = 3000;
    Float_t yMin = -3000;
    Float_t yMax = 3000;

    vector<vector<Double_t>> mesh = getMesh(hBinsN, vBinsN, xMin, xMax, yMin, yMax, tree);

    TH2F *hist = new TH2F("h1", "Protons Energy Deposition; x [mm]; y[mm]", hBinsN, xMin, xMax, vBinsN, yMin, yMax);

    for(Int_t i=0; i<hBinsN; ++i)
    {
        for(Int_t j=0; j<vBinsN; ++j)
        {
            hist->SetBinContent(i,j,mesh[i][j]);
        }
    }

    hist->Draw("colz");

}