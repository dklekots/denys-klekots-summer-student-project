#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"
#include "vector"
#include "TBranch.h"
#include "TH2F.h"
#include "iostream"
#include "TCanvas.h"

using namespace::std;

void BuildPlots_eDep_vs_trkLen()
{
    TFile *file_prot = new TFile("TestUserActionOutput_proton.root");
    TTree *tree_prot = (TTree*)file_prot->Get("MyOutput;1");
    TCanvas *c1 = new TCanvas();
    c1->Divide(2,1);
    c1->cd(1);
    TH2F *hist_prot_trkLen = new TH2F("hist_prot_trkLen", "Primary Protons; Track Length [mm]; E Dep. [MeV]", 
                                1000, 0.0, 0.1, 1000, 0.0, 4.2);
    tree_prot->Draw("eDep:trkLen>>hist_prot_trkLen", "trkID == 1", "colz");
    c1->cd(2);
    TH2F *hist_prot_X = new TH2F("hist_prot_X", "Primary Protons; X [mm]; E Dep. [MeV]", 
                                1000, 0.0, 0.1, 1000, 0.0, 4.2);
    tree_prot->Draw("eDep:postX>>hist_prot_X", "trkID == 1", "colz");

    TFile *file_electr = new TFile("TestUserActionOutput_e-.root");
    TTree *tree_electr = (TTree*)file_electr->Get("MyOutput;1");
    TCanvas *c2 = new TCanvas();
    c2->Divide(2,1);
    c2->cd(1);
    TH2F *hist_electr_trkLen = new TH2F("hist_electr_trkLen", "Primary Electrons; Track Length [mm]; E Dep. [MeV]",
                                1000, 0.0, 4.0, 1000, 0.0, 0.2);
    tree_electr->Draw("eDep:trkLen>>hist_electr_trkLen", "trkID == 1", "colz");  
    c2->cd(2);
    TH2F *hist_electr_X = new TH2F("hist_electr_X", "Primary Electrons; X [mm]; E Dep. [MeV]", 
                                1000, 0.0, 2.0, 1000, 0.0, 0.2);
    tree_electr->Draw("eDep:postX>>hist_electr_X", "trkID == 1", "colz");


    // TFile *file_mu = new TFile("TestUserActionOutput_mu-.root");
    // TTree *tree_mu = (TTree*)file_mu->Get("MyOutput;1");
    // TCanvas *c3 = new TCanvas();
    // c3->Divide(1,2);
    // c3->cd(1);
    // TH2F *hist_mu_trkLen = new TH2F("hist_mu_trkLen", "Primary Muons 4 MeV; Track Length [mm]; E Dep. [MeV]",
    //                             1000, 0.0, 0.5, 1000, 0.0, 4.5);
    // tree_mu->Draw("eDep:trkLen>>hist_mu_trkLen", "trkID == 1", "colz");  
    // c3->cd(2);
    // TH2F *hist_mu_X = new TH2F("hist_mu_X", "Primary Muons 4 MeV; X [mm]; E Dep. [MeV]", 
    //                             1000, 0.0, 0.5, 1000, 0.0, 4.5);
    // tree_mu->Draw("eDep:postX>>hist_mu_X", "trkID == 1", "colz");


    TFile *file_e_old = new TFile("TestUserActionOutput_e-_old.root");
    TTree *tree_e_old = (TTree*)file_e_old->Get("MyOutput;1");
    TCanvas *c4 = new TCanvas();
    c4->Divide(2,1);
    c4->cd(1);
    TH2F *hist_e_old_trkLen = new TH2F("hist_e_old_trkLen", "Primary Electrons, FTFP_BERT Physics List; Track Length [mm]; E Dep. [MeV]",
                                1000, 0.0, 4, 1000, 0.0, 3.0);
    tree_e_old->Draw("eDep:trkLen>>hist_e_old_trkLen", "trkID == 1", "colz");  
    c4->cd(2);
    TH2F *hist_e_old_X = new TH2F("hist_e_old_X", "Primary Electrons, FTFP_BERT Physics Ligt; X [mm]; E Dep. [MeV]", 
                                1000, 0.0, 1.6, 1000, 0.0, 3.0);
    tree_e_old->Draw("eDep:postX>>hist_e_old_X", "trkID == 1", "colz");


    TCanvas *cTest = new TCanvas();
    TH1F* histTest = new TH1F("histSteps", "Step Length for Bad electrons; Step Length[mm]; Entries", 1000, 0.0, 1.0);
    tree_e_old->Draw("TMath::Sqrt((preX-postX)**2+(preY-postY)**2+(preZ-postZ)**2)>>histSteps", "trkID == 1");
}