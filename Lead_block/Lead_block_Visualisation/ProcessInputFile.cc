#include "TFile.h"
#include "TTree.h"

#include <vector>
#include <map>
#include <iostream>

using namespace ::std;

struct trackData
{
  vector<float> xCoors;
  vector<float> yCoors;
  vector<float> zCoors;
  vector<float> time;
  int particlePDG;
};

struct eventData
{
  map<int, trackData> tracksMap;
};

map<int, eventData> eventsMap;

void ProcessInputFile()
{
  TFile *file = new TFile("TestUserActionOutput_proton_high_energy.root");
  TTree *tree = (TTree *)file->Get("MyOutput;1");

  TBranch *eventIDbranch = tree->GetBranch("eventID");
  TBranch *trkIDbranch = tree->GetBranch("trkID");
  TBranch *particlePDGbranch = tree->GetBranch("particlePDG");
  TBranch *preXbranch = tree->GetBranch("preX");
  TBranch *preYbranch = tree->GetBranch("preY");
  TBranch *preZbranch = tree->GetBranch("preZ");
  TBranch *preTimeBranch = tree->GetBranch("preTime");
  TBranch *postXbranch = tree->GetBranch("postX");
  TBranch *postYbranch = tree->GetBranch("postY");
  TBranch *postZbranch = tree->GetBranch("postZ");
  TBranch *postTimeBranch = tree->GetBranch("postTime");

  Int_t eventID;
  Int_t trkID;
  Int_t particlePDG;
  Float_t preX;
  Float_t preY;
  Float_t preZ;
  Float_t preTime;
  Float_t postX;
  Float_t postY;
  Float_t postZ;
  Float_t postTime;

  eventIDbranch->SetAddress(&eventID);
  trkIDbranch->SetAddress(&trkID);
  particlePDGbranch->SetAddress(&particlePDG);
  preXbranch->SetAddress(&preX);
  preYbranch->SetAddress(&preY);
  preZbranch->SetAddress(&preZ);
  preTimeBranch->SetAddress(&preTime);
  postXbranch->SetAddress(&postX);
  postYbranch->SetAddress(&postY);
  postZbranch->SetAddress(&postZ);
  postTimeBranch->SetAddress(&postTime);

  Int_t entriesN = tree->GetEntries();

  for (int i = 0; i < entriesN; ++i)
  {
    tree->GetEntry(i);

    if (eventsMap.find(eventID) != eventsMap.end())
    {
      if (eventsMap[eventID].tracksMap.find(trkID) != eventsMap[eventID].tracksMap.end())
      {
        eventsMap[eventID].tracksMap[trkID].xCoors.push_back(postX);
        eventsMap[eventID].tracksMap[trkID].yCoors.push_back(postY);
        eventsMap[eventID].tracksMap[trkID].zCoors.push_back(postZ);
        eventsMap[eventID].tracksMap[trkID].time.push_back(postTime);
      }
      else
      {
        trackData tmpTrackData;

        tmpTrackData.xCoors.push_back(preX);
        tmpTrackData.xCoors.push_back(postX);

        tmpTrackData.yCoors.push_back(preY);
        tmpTrackData.yCoors.push_back(postY);

        tmpTrackData.zCoors.push_back(preZ);
        tmpTrackData.zCoors.push_back(postZ);

        tmpTrackData.time.push_back(preTime);
        tmpTrackData.time.push_back(postTime);

        tmpTrackData.particlePDG = particlePDG;

        eventsMap[eventID].tracksMap.emplace(trkID, tmpTrackData);
      }
    }
    else
    {
      eventData tmpEventData;
      trackData tmpTrackData;

      tmpTrackData.xCoors.push_back(preX);
      tmpTrackData.xCoors.push_back(postX);

      tmpTrackData.yCoors.push_back(preY);
      tmpTrackData.yCoors.push_back(postY);

      tmpTrackData.zCoors.push_back(preZ);
      tmpTrackData.zCoors.push_back(postZ);

      tmpTrackData.time.push_back(preTime);
      tmpTrackData.time.push_back(postTime);

      tmpTrackData.particlePDG = particlePDG;

      tmpEventData.tracksMap.emplace(trkID, tmpTrackData);

      eventsMap.emplace(eventID, tmpEventData);
    }
  }

  for (auto eventIter : eventsMap)
  {
    cout << "The event number is " << eventIter.first << endl;

    for (auto trkIter : eventIter.second.tracksMap)
    {
      cout << "\t The track number is " << trkIter.first << " particlePDG is " << trkIter.second.particlePDG << endl;

      const vector<float> &xCoors = trkIter.second.xCoors;
      const vector<float> &yCoors = trkIter.second.yCoors;
      const vector<float> &zCoors = trkIter.second.zCoors;
      const vector<float> &time = trkIter.second.time;

      int stepsNum = xCoors.size();

      for (int i = 0; i < stepsNum; i++)
      {
        cout << "\t\t x = " << xCoors[i] << "\t y = " << yCoors[i] << "\t z = " << zCoors[i] << "\t time = " << time[i] << endl;
      }
    }
  }
}
