//Geomode includes
#include "GeoModelKernel/GeoElement.h"
#include "GeoModelKernel/GeoMaterial.h"
#include "GeoModelKernel/GeoBox.h"
#include "GeoModelKernel/GeoLogVol.h"
#include "GeoModelKernel/GeoPhysVol.h"
#include "GeoModelKernel/GeoTransform.h"
#include "GeoModelKernel/GeoNameTag.h"
#include "GeoModelDBManager/GMDBManager.h"
#include "GeoModelWrite/WriteGeoModel.h"
#include "GeoModelKernel/GeoNameTag.h"

//  C++ includes
#include <string>
#include <fstream>
#include <cstdlib> // EXIT_FAILURE

// Units
#include "GeoModelKernel/Units.h"
#define SYSTEM_OF_UNITS GeoModelKernelUnits // so we will get, e.g., 'GeoModelKernelUnits::cm'

// Define the units
#define gr   SYSTEM_OF_UNITS::gram
#define mole SYSTEM_OF_UNITS::mole
#define cm3  SYSTEM_OF_UNITS::cm3
#define cm   SYSTEM_OF_UNITS::cm

int main(int argc, char *argv[])
{


    //-----------------------------------------------------------------------------------//
	// Define the materials that we shall use.                                           //
	// ----------------------------------------------------------------------------------//

    GeoElement* nitrogen = new GeoElement("Nitrogen", "N2", 7.0, 14.0*gr/mole);
    GeoElement* lead = new GeoElement("Lead", "Pb", 82, 207.2*gr/mole);

    GeoMaterial* vacuum = new GeoMaterial("Vacuum", 1E-8*gr/cm3);
    vacuum->add(nitrogen);
    vacuum->lock();

    GeoMaterial* metalLead = new GeoMaterial("Metal Lead", 11.35*gr/cm3);
    metalLead->add(lead);
    metalLead->lock();

    // GeoMaterial* metalLeadSenc = new GeoMaterial("Metal Lead Sensitive", 11.35*gr/cm3);
    // metalLeadSenc->add(lead);
    // metalLeadSenc->lock();

    //Create a world volume
    const GeoBox* sWorld = new GeoBox(300*cm, 300*cm, 300*cm);
    const GeoLogVol* lWorld = new GeoLogVol("WorldLV", sWorld, vacuum);
    GeoPhysVol* pWorld = new GeoPhysVol(lWorld);


    //Creatr a Lead Cube volume
    const GeoBox* sCube = new GeoBox(100*cm, 100*cm, 100*cm);
    const GeoLogVol* lCube = new GeoLogVol("LeadCubeLV", sCube, metalLead);
    GeoPhysVol* pCube = new GeoPhysVol(lCube);

    //make transformation for cube
    GeoTrf::Translate3D tr(100*cm, 0, 0);
    GeoTransform *trCube = new GeoTransform(tr);

    //create world volume name tag
    GeoNameTag* cubeNameTag = new GeoNameTag("Lead Cube");

    //add Cube to the world volume with specific name
    pWorld->add(trCube);
    pWorld->add(cubeNameTag);
    pWorld->add(pCube);

	// //------------------------------------------------------------------------------------//
	// // Creating and inserting slices
	// //------------------------------------------------------------------------------------//
    // int numOfSensSlices = 10; // number of slices of one sort (i.e sensitive or ordinary)
    // const GeoBox* sSlice = new GeoBox(100.0*cm/numOfSensSlices/2, 100*cm, 100*cm);
    // const GeoLogVol* lSlceSens = new GeoLogVol( "SliceSencLV", sSlice, metalLeadSenc);
    // GeoPhysVol* pSliceSens = new GeoPhysVol(lSlceSens);

    // for(int i=0; i<numOfSensSlices; ++i)
    // {
    //     GeoTrf::Translate3D trIter( 100*cm/numOfSensSlices*2*(i-4.25), 0, 0);
    //     GeoTransform* trSloceIter = new GeoTransform(trIter);
    //     pCube->add(trSloceIter);
    //     pCube->add(pSliceSens);
    // }


	//------------------------------------------------------------------------------------//
	// Writing the geometry to file
	//------------------------------------------------------------------------------------//
    std::string path = "../LeadCube.db";

    // check if DB exist. If not, return.
    //FIXME: TODO: this check should go in the 'GMDBManager' constructor.
    std::ifstream infile(path.c_str());
    if (infile.good())
    {
        std::cout << "\n\tWARNING!! A file '" << path <<"' already exists!!"
                  << "Removing the file \n" << std::endl;
        std::string comand= "rm ./";
        comand += path;
        system(comand.c_str());
    }
    infile.close();

    // open the DB connection
    GMDBManager db(path);

    // check the DB connection
    if(db.checkIsDBOpen())
    {
        std::cout << "OK! Database in opene!" << std::endl;
    }
    else
    {
        std::cout << "Daatabase ERROR!! Exitin..." << std::endl;
    }

    // Dump the tree volumes into local file
    GeoModelIO::WriteGeoModel dumpGeoModelGraph(db);
    pWorld->exec(&dumpGeoModelGraph);
    dumpGeoModelGraph.saveToDB();

    return 0;

}
