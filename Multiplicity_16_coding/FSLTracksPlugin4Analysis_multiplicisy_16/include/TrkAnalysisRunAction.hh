#ifndef TRKANALYSISRUNACTION_HH
#define TRKANALYSISRUNACTION_HH

#include "G4UserRunAction.hh"
#include "G4UnitsTable.hh"
#include <vector>

class TrkAnalysisRunAction: public G4UserRunAction
{
public:

    TrkAnalysisRunAction();
    ~TrkAnalysisRunAction();

    void BeginOfRunAction(const G4Run* /*aRun*/);
    void EndOfRunAction(const G4Run* /*aRun*/);

    static void SetStepPointX(G4float value)
        {fStepPointsX.push_back(value);}
    static void SetStepPointY(G4float value)
        {fStepPointsY.push_back(value);}
    static void SetStepPointZ(G4float value)
        {fStepPointsZ.push_back(value);}
    static void SetStepPointTime(G4float value)
        {fStepPointsTime.push_back(value);}

    static void ClearStepPoints();

    G4ThreadLocal static G4int trkNtupleID;

private:

    G4ThreadLocal static std::vector<G4float> fStepPointsX;
    G4ThreadLocal static std::vector<G4float> fStepPointsY;
    G4ThreadLocal static std::vector<G4float> fStepPointsZ;
    G4ThreadLocal static std::vector<G4float> fStepPointsTime;

};



#endif // TRKANALYSISRUNACTION_HH