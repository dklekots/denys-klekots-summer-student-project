#ifndef __TRKANALYSISTRACKINGACTION_HH__
#define __TRKANALYSISTRACKINGACTION_HH__

#include "G4UserTrackingAction.hh"

class TrkAnalysisTrackingAction: public G4UserTrackingAction
{
public:
    TrkAnalysisTrackingAction();
    ~TrkAnalysisTrackingAction();

    void PreUserTrackingAction(const G4Track*);
    void PostUserTrackingAction(const G4Track*);

};


#endif //__TRKANALYSISTRACKINGACTION_HH__