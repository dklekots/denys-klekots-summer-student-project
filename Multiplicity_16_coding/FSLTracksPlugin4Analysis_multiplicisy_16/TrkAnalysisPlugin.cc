#include "FullSimLight/FSLUserActionPlugin.h"
#include <iostream>

#include "TrkAnalysisRunAction.hh"
#include "TrkAnalysisSteppingAction.hh"
#include "TrkAnalysisTrackingAction.hh"

class TrkAnalysisPlugin:public FSLUserActionPlugin {
    
public:
    
    TrkAnalysisPlugin();
    // virtual const G4VUserActionInitialization *getUserActionInitialization() const final;

    G4UserRunAction* getRunAction() const;
    G4UserSteppingAction* getSteppingAction() const;
    G4UserTrackingAction* getTrackingAction() const;
};

TrkAnalysisPlugin::TrkAnalysisPlugin()
{

}

G4UserRunAction* TrkAnalysisPlugin::getRunAction() const
{
    return new TrkAnalysisRunAction();
}

G4UserSteppingAction* TrkAnalysisPlugin::getSteppingAction() const
{
    return new TrkAnalysisSteppingAction();
}

G4UserTrackingAction* TrkAnalysisPlugin::getTrackingAction() const
{
    return new TrkAnalysisTrackingAction();
}

extern "C" TrkAnalysisPlugin *createTrkAnalysisPlugin() {
    return new TrkAnalysisPlugin();
}

