#include "TrkAnalysisSteppingAction.hh"
#include "G4Step.hh"
#include "G4Track.hh"
#include "G4UnitsTable.hh"
#include "G4AnalysisManager.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"
#include "TrkAnalysisRunAction.hh"
#include "G4VPhysicalVolume.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrkAnalysisSteppingAction::TrkAnalysisSteppingAction(): G4UserSteppingAction()
{

}
 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrkAnalysisSteppingAction::~TrkAnalysisSteppingAction()
{
    // ---
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TrkAnalysisSteppingAction::UserSteppingAction(const G4Step * aStep)
{
    G4ThreeVector postStepPoint = aStep->GetPostStepPoint()->GetPosition();
    G4float postTime = aStep->GetPostStepPoint()->GetGlobalTime();

    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

    TrkAnalysisRunAction::SetStepPointX(postStepPoint.x());
    TrkAnalysisRunAction::SetStepPointY(postStepPoint.y());
    TrkAnalysisRunAction::SetStepPointZ(postStepPoint.z());
    TrkAnalysisRunAction::SetStepPointTime(postTime);



    //the lines below were written to special simulation, one can remove them
    G4VPhysicalVolume* pVol = aStep->GetPostStepPoint()->GetTouchable()
                                  ->GetVolume();

    if(pVol && pVol->GetLogicalVolume()->GetName() == "WorldLog")
    {
        aStep->GetTrack()->SetTrackStatus(fStopAndKill);
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[11]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,11);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[12]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,12);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[13]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,13);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[14]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,14);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[21]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,21);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[22]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,22);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[23]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,23);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());            
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[24]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,24);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[31]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,31);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[32]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,32);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[33]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,33);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[34]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,34);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[41]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,41);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[42]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,42);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[43]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,43);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }
    else if (pVol && pVol->GetLogicalVolume()->GetName() == "AFP03_Q1_LogTDSensor[44]" )
    {
        analysisManager->FillNtupleIColumn(TrkAnalysisRunAction::trkNtupleID,1,44);
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,7,postStepPoint.y());
        analysisManager->FillNtupleFColumn(TrkAnalysisRunAction::trkNtupleID,8,postStepPoint.z());
    }


}