#include "ExamplePrimaryGeneratorAction.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "G4AnalysisManager.hh"
#include "G4UnitsTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ExamplePrimaryGeneratorAction::ExamplePrimaryGeneratorAction()
 : G4VUserPrimaryGeneratorAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ExamplePrimaryGeneratorAction::~ExamplePrimaryGeneratorAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ExamplePrimaryGeneratorAction::GeneratePrimaries(G4Event* event)
{

  std::vector<G4String> particleNames = {"proton", "neutron", "e-", "e+", "gamma", 
                                         "pi+", "mu+", "kaon+", "nu_e"};

  G4double thetaAxis = G4UniformRand()*360*deg;
  G4double phiAxis   = G4UniformRand()*360*deg;
  G4double psiAxis   = G4UniformRand()*360*deg; 
  G4ThreeVector axis(1, 0, 0);
  axis.rotateZ(phiAxis);
  axis.rotateX(thetaAxis);
  axis.rotateZ(psiAxis);

  G4double thetaDirection = G4UniformRand()*360*deg;
  G4double phiDirection   = G4UniformRand()*360*deg;
  G4double psiDirection   = G4UniformRand()*360*deg; 
  G4ThreeVector direction(0,1,0);
  direction.rotateZ(phiDirection);
  direction.rotateX(thetaDirection);
  direction.rotateZ(psiDirection);

  for (long unsigned int i = 0; i < particleNames.size(); ++i)
  {
    // Define particle properties
    G4String particleName = particleNames[i];
    G4ThreeVector position(0, 0, 0);   
    G4double kinEnergy = 10*MeV;
    G4double time = 0;

    direction.rotate(axis, 40*deg);

    // Get particle definition from G4ParticleTable
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition* particleDefinition 
      = particleTable->FindParticle(particleName);
    if ( ! particleDefinition ) {
      G4cerr << "Error: " << particleName << " not found in G4ParticleTable" << G4endl;
      exit(1);
    }  
      
    // Create primary particle
    G4PrimaryParticle* primaryParticle = new G4PrimaryParticle(particleDefinition);
    primaryParticle->SetKineticEnergy(kinEnergy);
    primaryParticle->SetMomentumDirection(direction);
    primaryParticle->SetMass(particleDefinition->GetPDGMass());
    primaryParticle->SetCharge( particleDefinition->GetPDGCharge());

    // Create vertex 
    G4PrimaryVertex* vertex = new G4PrimaryVertex(position, time);
    vertex->SetPrimary(primaryParticle);
    event->AddPrimaryVertex(vertex);
  }
}
