The example fullSimLight plugin for user primary generator action. This plugin generates 9 tracks of 9 different particles, as shown in figure
[](https://gitlab.cern.ch/dklekots/denys-klekots-summer-student-project/-/blob/master/FSLExamplePrimaryGeneratorPlugin/The_picture_shows_how_does_plugin_generate%20events..png)

![](./The_picture_shows_how_does_plugin_generate%20events..png "Image Title")

The plugin shows an example of how to make a user's custom generator action (based on Geant4 `G4VUserPrimaryGeneratorAction` class), which can be very useful, for example in case it needed to create particle source properties defined in an external file, or write data from particle source into output analysis file.


Run in terminal the following commands for building plugin
```
mkdir build
cd build
cmake ../
make -j
```
