# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

################################################################################
# Package: MinimalPlugin
# author: Riccardo Maria BIANCHI @ CERN - May, 2022
################################################################################

cmake_minimum_required(VERSION 3.1.0)
project(AFP_Geometry_plugin)

# Compile with C++17
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS ON)


# Find the needed dependencies, when building individually
  message (${CMAKE_SOURCE_DIR}) 
  message (${PROJECT_SOURCE_DIR})
  
if ( CMAKE_SOURCE_DIR STREQUAL PROJECT_SOURCE_DIR ) # when buildingindividually
   find_package( GeoModelCore REQUIRED  ) 
   find_package( GeoModelG4 REQUIRED )
   find_package( Geant4 REQUIRED )
   find_package( CLHEP REQUIRED)
endif()


# Find the header and source files.
file( GLOB SOURCES src/*.cxx )
file( GLOB HEADERS include/*.h )

add_subdirectory(GeoModelUtilities)
add_subdirectory(AFP_Geometry)

add_library( MinimalPlugin SHARED ${SOURCES} ${HEADERS} MinimalPlugin.cxx)

target_link_libraries( MinimalPlugin PUBLIC ${CLHEP_LIBRARIES} GeoModelCore::GeoModelKernel GeoMaterial2G4 GeoModelUtilities AFP_Geometry)

target_include_directories( MinimalPlugin PUBLIC
   $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
   $<INSTALL_INTERFACE:include> ${CLHEP_INCLUDE_DIRS} $ENV{GEOMODEL_INCLUDE_DIR} 
                           ${CMAKE_CURRENT_SOURCE_DIR}/GeoModelUtilities/ ${CMAKE_CURRENT_SOURCE_DIR}/AFP_Geometry/)

source_group( "src" FILES ${SOURCES} )

set_target_properties( MinimalPlugin PROPERTIES
   VERSION ${PROJECT_VERSION}
   SOVERSION ${PROJECT_VERSION_MAJOR} )

# Install the library.
install( TARGETS MinimalPlugin
   EXPORT ${PROJECT_NAME}-export
   LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
   COMPONENT Runtime
   NAMELINK_SKIP )

