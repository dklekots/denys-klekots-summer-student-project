//Geomodel includes
#include "GeoMaterial2G4/GeoExtendedMaterial.h"
#include "GeoModelKernel/GeoMaterial.h"
#include "GeoModelKernel/GeoBox.h"
#include "GeoModelKernel/GeoShapeShift.h"
#include "GeoModelKernel/GeoShapeUnion.h"
#include "GeoModelKernel/GeoLogVol.h"
#include "GeoModelKernel/GeoPhysVol.h"
#include "GeoModelKernel/GeoTrap.h"
#include "GeoModelKernel/GeoTransform.h"

#include "GeoModelDBManager/GMDBManager.h"
#include "GeoModelWrite/WriteGeoModel.h"

// #include "GeoMaterial2G4/GeoMaterialPropertiesTable.h"

//  C++ includes
#include <string>
#include <fstream>

// Units
#include "GeoModelKernel/Units.h"
#define SYSTEM_OF_UNITS GeoModelKernelUnits // so we will get, e.g., 'GeoModelKernelUnits::cm'

// Define the units
#define g       SYSTEM_OF_UNITS::gram
#define mole    SYSTEM_OF_UNITS::mole
#define cm3     SYSTEM_OF_UNITS::cm3
#define cm      SYSTEM_OF_UNITS::cm
#define mm      SYSTEM_OF_UNITS::mm
#define deg     SYSTEM_OF_UNITS::degree
#define eV      SYSTEM_OF_UNITS::electronvolt

// class GeoExtendedMaterial;

int main(int argc, char *argv[])
{

    //-----------------------------------------------------------------------------------//
	// Define the materials that we shall use.                                           //
	// ----------------------------------------------------------------------------------//

    GeoElement *silicon = new GeoElement("Silicon", "Si", 14, 28.0855*g/mole);
    GeoElement *oxygen = new GeoElement("Oxygen", "O", 8, 15.9994*g/mole);

    GeoExtendedMaterial *quartz = new GeoExtendedMaterial("Quartz",  2.203*g/cm3);
    quartz->add(silicon, 28.0855);
    quartz->add(oxygen, 2*15.9994);
    quartz->lock();

    GeoExtendedMaterial *vacuum = new GeoExtendedMaterial("Vacuum", 1E-8*g/cm3);
    vacuum->add(oxygen, 1);
    vacuum->lock();

    //define quartz material property 
    double quartz_Energy[] = { 7.0 * eV, 7.07 * eV, 7.14 * eV };
    double quartz_RIND[]  = { 1.59, 1.57, 1.54 };
    double quartz_AbsLength[] = { 420. * cm, 420. * cm, 420. * cm };
    GeoMaterialPropertiesTable* quartz_mt = new GeoMaterialPropertiesTable();
    quartz_mt->AddProperty("RINDEX", quartz_Energy, quartz_RIND, 3);
    quartz_mt->AddProperty("ABSLENGTH", quartz_Energy, quartz_AbsLength, 3);
    quartz_mt->AddConstProperty("RESOLUTIONSCALE", 0.1);
    quartz->SetMaterialPropertiesTable(quartz_mt);


    //defiene vacuum material property
    double vacuum_Energy[] = { 7.0 * eV, 7.07 * eV, 7.14 * eV };
    double vacuum_AbsLength[] = { 1E10, 1E10, 1E10 };
    double vacuum_RIND[]  = { 1, 1, 1};  
    GeoMaterialPropertiesTable* vacuum_mt = new GeoMaterialPropertiesTable();  
    vacuum_mt->AddProperty("RINDEX", vacuum_Energy, vacuum_RIND, 3);
    vacuum_mt->AddProperty("ABSLENGTH", vacuum_Energy, vacuum_AbsLength, 3);
    vacuum_mt->AddConstProperty("RESOLUTIONSCALE", 0.1);
    vacuum->SetMaterialPropertiesTable(vacuum_mt);



    //Create a world volume
    const GeoBox *sWorld = new GeoBox(100*mm, 100*mm, 100*mm);
    const GeoLogVol *lWorld = new GeoLogVol("WorldLV", sWorld, vacuum);
    GeoPhysVol *pWorld = new GeoPhysVol(lWorld);
    
    //creatr Radiator shape
    const GeoTrap *sRadiator = new GeoTrap(3*mm, 24.22775*deg, 270*deg, 27.05*mm, 2.5*mm,
                                           2.5*mm, 0, 24.35*mm, 2.5*mm, 2.5*mm, 0);


    //create light guide shape
    const GeoTrap *sLightGuide = new GeoTrap(2.5*mm, 26.565*deg, 270*deg, 35.1*mm, 
                                             3*mm, 3*mm, 0, 32.6*mm, 3*mm, 3*mm, 0);

    //create transformation of light guide before shape union
    GeoTrf::RotateY3D ShapeLightGuideRotateY(-90*deg);
    GeoTrf::RotateZ3D ShapeLightGuideRotateZ(90*deg);
    GeoTrf::Translate3D ShapeLightGuideTranslate3D( 33.85*mm, -28.2*mm+1E-5*mm, 0);
    //The correction in y coordinate for program, to not insert world bolume between
    //light guide and radiator

    //chreate shifted light guide and apply all transformation 
    //(in classes inplemented autodelete)
    GeoShapeShift *lightGuideShift = new GeoShapeShift(sLightGuide, ShapeLightGuideRotateY);
    lightGuideShift = new GeoShapeShift(lightGuideShift, ShapeLightGuideRotateZ);
    lightGuideShift = new GeoShapeShift(lightGuideShift, ShapeLightGuideTranslate3D);

    //create Train2BarALV volume
    const GeoShapeUnion *sTrain2BarA = new GeoShapeUnion(sRadiator, lightGuideShift);
    const GeoLogVol* lTrain2BarA = new GeoLogVol("Train2BarALV", sTrain2BarA, quartz);
    GeoPhysVol * pTrain2BarA = new GeoPhysVol(lTrain2BarA);

    GeoTrf::Translate3D coorTranslate(2.5*mm, 28.4*mm, 3*mm);
    GeoTrf::RotateZ3D coorRotate(180*deg);
    GeoTransform *cootTransorm1 = new GeoTransform(coorTranslate);
    GeoTransform *cootTransorm2 = new GeoTransform(coorRotate);
    pWorld->add(cootTransorm1);
    pWorld->add(cootTransorm2);


    pWorld->add(pTrain2BarA);

    //------------------------------------------------------------------------------------//
	// Writing the geometry to file
	//------------------------------------------------------------------------------------//
    std::string path = "../Train2BarA.db";

    // check if DB exist. If not, return.
    //FIXME: TODO: this check should go in the 'GMDBManager' constructor.
    std::ifstream infile(path.c_str());
    if (infile.good())
    {
        std::cout << "\n\tWARNING!! A file '" << path <<"' already exists!!"
                  << "Removing the file \n" << std::endl;
        std::string comand= "rm ./";
        comand += path;
        system(comand.c_str());
    }
    infile.close();

    // open the DB connection
    GMDBManager db(path);

    // check the DB connection
    if(db.checkIsDBOpen())
    {
        std::cout << "OK! Database in opene!" << std::endl;
    }
    else
    {
        std::cout << "Daatabase ERROR!! Exitin..." << std::endl;
    }

    // Dump the tree volumes into local file
    GeoModelIO::WriteGeoModel dumpGeoModelGraph(db);
    pWorld->exec(&dumpGeoModelGraph);
    dumpGeoModelGraph.saveToDB();

    return 0;
}