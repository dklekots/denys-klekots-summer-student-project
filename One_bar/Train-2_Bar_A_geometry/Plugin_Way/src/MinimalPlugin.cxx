#include "GeoModelKernel/GeoVGeometryPlugin.h"

// Geomodel includes
#include "GeoMaterial2G4/GeoExtendedMaterial.h"
#include "GeoModelKernel/GeoMaterial.h"
#include "GeoModelKernel/GeoBox.h"
#include "GeoModelKernel/GeoPara.h"
#include "GeoModelKernel/GeoShapeShift.h"
#include "GeoModelKernel/GeoShapeUnion.h"
#include "GeoModelKernel/GeoLogVol.h"
#include "GeoModelKernel/GeoPhysVol.h"
#include "GeoModelKernel/GeoTrap.h"
#include "GeoModelKernel/GeoTransform.h"

#include "GeoModelDBManager/GMDBManager.h"
#include "GeoModelWrite/WriteGeoModel.h"

// #include "GeoMaterial2G4/GeoMaterialPropertiesTable.h"

//  C++ includes
#include <string>
#include <fstream>

// Units
#include "GeoModelKernel/Units.h"
#define SYSTEM_OF_UNITS GeoModelKernelUnits // so we will get, e.g., 'GeoModelKernelUnits::cm'

// Define the units
#define g SYSTEM_OF_UNITS::gram
#define mole SYSTEM_OF_UNITS::mole
#define cm3 SYSTEM_OF_UNITS::cm3
#define cm SYSTEM_OF_UNITS::cm
#define mm SYSTEM_OF_UNITS::mm
#define deg SYSTEM_OF_UNITS::degree
#define eV SYSTEM_OF_UNITS::electronvolt

// Class Declaration

class MinimalPlugin : public GeoVGeometryPlugin
{

public:
    // Constructor:
    MinimalPlugin();

    // Destructor:
    ~MinimalPlugin();

    // Creation of geometry:
    virtual void create(GeoPhysVol *world, bool publish = false);

private:
    // Illegal operations:
    const MinimalPlugin &operator=(const MinimalPlugin &right) = delete;
    MinimalPlugin(const MinimalPlugin &right) = delete;
};

// Class definition:

// Constructor
MinimalPlugin::MinimalPlugin()
{
}

// Destructor
MinimalPlugin::~MinimalPlugin()
{
}

// The create algorithm creates a tree of physical volumes rooted under the
// "world" physical volume. The optional flag publish is not used in this
// example (normally one may "publish" a list of FullPhysVol's and Alignable
// transforms, but this example has none such).
//
void MinimalPlugin::create(GeoPhysVol *globalWorld, bool /*publish*/)
{
    //-----------------------------------------------------------------------------------//
    // Define the materials that we shall use.                                           //
    // ----------------------------------------------------------------------------------//

    GeoElement *silicon = new GeoElement("Silicon", "Si", 14, 28.0855 * g / mole);
    GeoElement *oxygen = new GeoElement("Oxygen", "O", 8, 15.9994 * g / mole);

    GeoExtendedMaterial *quartz = new GeoExtendedMaterial("Quartz", 2.203 * g / cm3);
    quartz->add(silicon, 28.0855);
    quartz->add(oxygen, 2 * 15.9994);
    quartz->lock();

    GeoExtendedMaterial *vacuum = new GeoExtendedMaterial("Vacuum", 1E-8 * g / cm3);
    vacuum->add(oxygen, 1);
    vacuum->lock();

    //any material to define absorber of photons at the end on light guide
    GeoExtendedMaterial *pmtAbsorber = new GeoExtendedMaterial("pmtAbsorber", 2.203 * g / cm3);
    pmtAbsorber->add(silicon, 28.0855);
    pmtAbsorber->add(oxygen, 2 * 15.9994);
    pmtAbsorber->lock();

    //any material to define absorber of photons at the end on light guide
    GeoExtendedMaterial *mirrorMaterial = new GeoExtendedMaterial("mirrorMaterial", 2.203 * g / cm3);
    mirrorMaterial->add(silicon, 28.0855);
    mirrorMaterial->add(oxygen, 2 * 15.9994);
    mirrorMaterial->lock();

    // define quartz material property
    double quartz_Energy[] = {7.0 * eV, 7.07 * eV, 7.14 * eV};
    double quartz_RIND[] = {1.59, 1.57, 1.54};
    double quartz_AbsLength[] = {420. * cm, 420. * cm, 420. * cm};
    GeoMaterialPropertiesTable *quartz_mt = new GeoMaterialPropertiesTable();
    quartz_mt->AddProperty("RINDEX", quartz_Energy, quartz_RIND, 3);
    quartz_mt->AddProperty("ABSLENGTH", quartz_Energy, quartz_AbsLength, 3);
    quartz_mt->AddConstProperty("RESOLUTIONSCALE", 0.1);
    quartz->SetMaterialPropertiesTable(quartz_mt);

    // defiene vacuum material property
    double vacuum_Energy[] = {7.0 * eV, 7.07 * eV, 7.14 * eV};
    double vacuum_AbsLength[] = {1E10*cm, 1E10*cm, 1E10*cm};
    double vacuum_RIND[] = {1, 1, 1};
    GeoMaterialPropertiesTable *vacuum_mt = new GeoMaterialPropertiesTable();
    vacuum_mt->AddProperty("RINDEX", vacuum_Energy, vacuum_RIND, 3);
    vacuum_mt->AddProperty("ABSLENGTH", vacuum_Energy, vacuum_AbsLength, 3);
    vacuum_mt->AddConstProperty("RESOLUTIONSCALE", 0.1);
    vacuum->SetMaterialPropertiesTable(vacuum_mt);

    // defiene mirror material property
    double mirror_Energy[] = {7.0 * eV, 7.07 * eV, 7.14 * eV};
    double mirror_AbsLength[] = {420. * cm, 420. * cm, 420. * cm};
    double mirror_RIND[] = {1E10, 1E10, 1E10};
    GeoMaterialPropertiesTable *mirror_mt = new GeoMaterialPropertiesTable();
    mirror_mt->AddProperty("RINDEX", mirror_Energy, mirror_RIND, 3);
    mirror_mt->AddProperty("ABSLENGTH", mirror_Energy, mirror_AbsLength, 3);
    mirror_mt->AddConstProperty("RESOLUTIONSCALE", 0.1);
    mirrorMaterial->SetMaterialPropertiesTable(mirror_mt);

    // Create a world volume
    const GeoBox *sWorld = new GeoBox(100 * mm, 100 * mm, 100 * mm);
    const GeoLogVol *lWorld = new GeoLogVol("WorldLV", sWorld, vacuum);
    GeoPhysVol *pWorld = new GeoPhysVol(lWorld);

    // creatr Radiator shape
    const GeoTrap *sRadiator = new GeoTrap(3 * mm, 24.22775 * deg, 270 * deg, 27.05 * mm, 2.5 * mm,
                                           2.5 * mm, 0, 24.35 * mm, 2.5 * mm, 2.5 * mm, 0);

    // create light guide shape
    const GeoTrap *sLightGuide = new GeoTrap(2.5 * mm, 26.565 * deg, 270 * deg, 35.1 * mm,
                                             3 * mm, 3 * mm, 0, 32.6 * mm, 3 * mm, 3 * mm, 0);

    // create transformation of light guide before shape union
    GeoTrf::RotateY3D shapeLightGuideRotateY(-90 * deg);
    GeoTrf::RotateZ3D shapeLightGuideRotateZ(90 * deg);
    GeoTrf::Translate3D shapeLightGuideTranslate3D(33.85 * mm, -28.2 * mm + 1E-5 * mm, 0);
    // The correction in y coordinate for program, to not insert world bolume between
    // light guide and radiator

    // chreate shifted light guide and apply all transformation
    //(in classes inplemented autodelete)
    GeoShapeShift *lightGuideShift = new GeoShapeShift(sLightGuide, shapeLightGuideRotateY);
    lightGuideShift = new GeoShapeShift(lightGuideShift, shapeLightGuideRotateZ);
    lightGuideShift = new GeoShapeShift(lightGuideShift, shapeLightGuideTranslate3D);

    // create Train2BarALV volume
    const GeoShapeUnion *sTrain2BarA = new GeoShapeUnion(sRadiator, lightGuideShift);
    const GeoLogVol *lTrain2BarA = new GeoLogVol("Train2BarALV", sTrain2BarA, quartz);
    GeoPhysVol *pTrain2BarA = new GeoPhysVol(lTrain2BarA);

    //create photon absorber at the end of light guide and add it to the pTrain2BarA
    const GeoBox *sPhotAbsorber = new GeoBox(1E-5*mm, 2.5*mm, 3*mm);
    const GeoLogVol *lPhotAbsorber = new GeoLogVol("PhotAbsorberLV", sPhotAbsorber, pmtAbsorber);
    GeoPhysVol *pPhotAbsorber = new GeoPhysVol(lPhotAbsorber);
    GeoTrf::Translate3D absorTranslate3D(67.7 * mm, -28.2 * mm + 1E-5 * mm, 0);
    GeoTransform *absorPlacement = new GeoTransform(absorTranslate3D);
    pTrain2BarA->add(absorPlacement);
    pTrain2BarA->add(pPhotAbsorber);

    //create elbow mirror and add it to the pTrain2BarA
    const GeoPara *sElbowMirror = new GeoPara(1E-5*mm, 2.5*mm, 3*mm, -45*deg, 0, 0);
    const GeoLogVol *lElbowMirror = new GeoLogVol("ElbowMirrorLV", sElbowMirror, mirrorMaterial);
    GeoPhysVol *pElbowMirror = new GeoPhysVol(lElbowMirror);
    GeoTrf::TranslateY3D mirrorTranslateY3D(-28.2 * mm + 1E-5 * mm);
    GeoTransform *mirrorPlacement = new GeoTransform(mirrorTranslateY3D);
    pTrain2BarA->add(mirrorPlacement);
    pTrain2BarA->add(pElbowMirror);


    GeoTrf::Translate3D coorTranslate(2.5 * mm, 28.4 * mm, 3 * mm);
    GeoTrf::RotateZ3D coorRotate(180 * deg);
    GeoTransform *cootTransorm1 = new GeoTransform(coorTranslate);
    GeoTransform *cootTransorm2 = new GeoTransform(coorRotate);
    pWorld->add(cootTransorm1);
    pWorld->add(cootTransorm2);

    pWorld->add(pTrain2BarA);

    globalWorld->add(pWorld);
}

// The name of this routine must correspond to the name of the class,
// and also to the name of the source code file (this file)

extern "C" MinimalPlugin *createMinimalPlugin()
{
    return new MinimalPlugin;
}
