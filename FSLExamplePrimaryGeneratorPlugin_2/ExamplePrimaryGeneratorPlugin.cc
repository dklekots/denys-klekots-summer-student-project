#include "FullSimLight/FSLUserActionPlugin.h"
#include <iostream>

#include "ExamplePrimaryGeneratorAction.hh"

class UserActionsPlugin:public FSLUserActionPlugin {
    
public:
    
    UserActionsPlugin();

    G4VUserPrimaryGeneratorAction *getPrimaryGeneratorAction() const;
};

UserActionsPlugin::UserActionsPlugin()
{
    std::cout<<"Hello from the MyUserGunActionsPlugin"<<std::endl;
}

G4VUserPrimaryGeneratorAction* UserActionsPlugin::getPrimaryGeneratorAction() const
{
    return new ExamplePrimaryGeneratorAction;
}

extern "C" UserActionsPlugin *createExamplePrimaryGeneratorPlugin() {
    return new UserActionsPlugin();
}

