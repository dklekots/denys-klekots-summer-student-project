#ifndef MYUSERACTION_HH
#define MYUSERACTION_HH


#include "G4UserSteppingAction.hh"
#include "G4Step.hh"

class G4Step;

class MySteppingAction: public G4UserSteppingAction
{
public:
    MySteppingAction();
    ~MySteppingAction();

    void UserSteppingAction(const G4Step* /*aStep*/);

};

#endif // MYUSERACTION_HH