#ifndef MYRUNACTION_HH
#define MYRUNACTION_HH

#include "G4UserRunAction.hh"
#include "G4UnitsTable.hh"


class MyRunAction: public G4UserRunAction
{
public:

    MyRunAction();
    ~MyRunAction();

    void BeginOfRunAction(const G4Run* /*aRun*/);
    void EndOfRunAction(const G4Run* /*aRun*/);

};



#endif // MYRUNACTION_HH