#include "MySteppingAction.hh"
#include "G4Step.hh"
#include "G4Track.hh"
#include "G4UnitsTable.hh"
#include "G4AnalysisManager.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

MySteppingAction::MySteppingAction(): G4UserSteppingAction()
{
    G4cout << "Hello from MySteppingAction()" << G4endl;
}
 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

MySteppingAction::~MySteppingAction()
{
    // ---
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MySteppingAction::UserSteppingAction(const G4Step * aStep)
{
    G4Track* aTrack = aStep->GetTrack();

    G4int eventNum = G4RunManager::GetRunManager()->GetCurrentEvent()->
                                                    GetEventID();
    G4int trkID = aTrack->GetTrackID();
    G4int particlePDG = aTrack->GetParticleDefinition()->GetPDGEncoding();
    G4ThreeVector preStepPoint = aStep->GetPreStepPoint()->GetPosition();
    G4ThreeVector postStepPoint = aStep->GetPostStepPoint()->GetPosition();
    G4float preTime = aStep->GetPreStepPoint()->GetGlobalTime();
    G4float postTime = aStep->GetPostStepPoint()->GetGlobalTime();
    G4float eDep = aStep->GetTotalEnergyDeposit();
    G4float EkinPre = aStep->GetPreStepPoint()->GetKineticEnergy();
    G4float EkinPost = aStep->GetPostStepPoint()->GetKineticEnergy();
    G4float trkLen = aStep->GetTrack()->GetTrackLength();

    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    
    analysisManager->FillNtupleIColumn(0, eventNum);
    analysisManager->FillNtupleIColumn(1, trkID);
    analysisManager->FillNtupleIColumn(2, particlePDG);
    analysisManager->FillNtupleFColumn(3, preStepPoint.x()/mm);
    analysisManager->FillNtupleFColumn(4, preStepPoint.y()/mm);
    analysisManager->FillNtupleFColumn(5, preStepPoint.z()/mm);
    analysisManager->FillNtupleFColumn(6, preTime/ns);
    analysisManager->FillNtupleFColumn(7, postStepPoint.x()/mm);
    analysisManager->FillNtupleFColumn(8, postStepPoint.y()/mm);
    analysisManager->FillNtupleFColumn(9, postStepPoint.z()/mm);
    analysisManager->FillNtupleFColumn(10, postTime/ns);
    analysisManager->FillNtupleFColumn(11, eDep/MeV);
    analysisManager->FillNtupleFColumn(12, EkinPre/MeV);
    analysisManager->FillNtupleFColumn(13, EkinPost/MeV);
    analysisManager->FillNtupleFColumn(14, trkLen/mm);
    analysisManager->AddNtupleRow();

//    const G4String volName = aStep->GetPostStepPoint()->GetTouchable()
//                                ->GetVolume()->GetLogicalVolume()->GetName();
//
//    if (volName == "WorldLog")
//    {
//        aTrack->SetTrackStatus(fStopAndKill);
//        return;
//    }
//
//    if (volName == "PhotAbsorberLV" )
//    {
//        // G4float time = aStep->GetPostStepPoint()->GetGlobalTime();
//        G4float time = aStep->GetTrack()->GetLocalTime();
//
//        analysisManager->FillNtupleIColumn(1, 0, eventNum);
//        analysisManager->FillNtupleIColumn(1, 1, trkID);
//        analysisManager->FillNtupleDColumn(1, 2, time/ps);
//        analysisManager->FillNtupleDColumn(1, 3, trkLen/mm);
//        analysisManager->AddNtupleRow(1);
//    }

}
