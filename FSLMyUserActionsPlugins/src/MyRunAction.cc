#include "MyRunAction.hh"
#include "G4AnalysisManager.hh"
#include "G4UnitsTable.hh"

MyRunAction::MyRunAction()
{
    G4cout << "Hello from MyRunAction()" << G4endl;

    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    analysisManager->SetDefaultFileType("root");
    analysisManager->SetVerboseLevel(1);
    analysisManager->SetNtupleMerging(true);   
    
    analysisManager->SetFileName("MyUserActionOutput");
    
    analysisManager->CreateNtuple("MyOutput", "Steps"); // NtupleID = 0

    analysisManager->CreateNtupleIColumn("eventID");    // columnID = 0
    analysisManager->CreateNtupleIColumn("trkID");      // columnID = 1
    analysisManager->CreateNtupleIColumn("particlePDG");// columnID = 2
    analysisManager->CreateNtupleFColumn("preX");       // columnID = 3
    analysisManager->CreateNtupleFColumn("preY");       // columnID = 4
    analysisManager->CreateNtupleFColumn("preZ");       // columnID = 5
    analysisManager->CreateNtupleFColumn("preTime");    // columnID = 6
    analysisManager->CreateNtupleFColumn("postX");      // columnID = 7
    analysisManager->CreateNtupleFColumn("postY");      // columnID = 8
    analysisManager->CreateNtupleFColumn("postZ");      // columnID = 9
    analysisManager->CreateNtupleFColumn("postTime");   // columnID = 10
    analysisManager->CreateNtupleFColumn("eDep");       // columnID = 11
    analysisManager->CreateNtupleFColumn("EkinPre");    // columnID = 12
    analysisManager->CreateNtupleFColumn("EkinPost");   // columnID = 13
    analysisManager->CreateNtupleFColumn("trkLen");     // cplumnID = 14
    analysisManager->FinishNtuple(0);


    analysisManager->CreateNtuple("PMT", "Tracks");     // NtupleID = 1
    analysisManager->CreateNtupleIColumn("eventID");    // columnID = 0
    analysisManager->CreateNtupleIColumn("trkID");      // columnID = 1
    analysisManager->CreateNtupleDColumn("trkTime");    // columnID = 2
    analysisManager->CreateNtupleDColumn("trkLength");  // columnID = 3
    analysisManager->FinishNtuple(1);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

MyRunAction::~MyRunAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MyRunAction::BeginOfRunAction(const G4Run* /*aRun*/)
{
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    analysisManager->OpenFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void MyRunAction::EndOfRunAction(const G4Run* /*aRun*/)
{
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    analysisManager->Write();
    analysisManager->CloseFile();
}
