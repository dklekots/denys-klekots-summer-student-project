#include "FullSimLight/FSLUserActionPlugin.h"
#include <iostream>

#include "MyRunAction.hh"
#include "MySteppingAction.hh"

class MyUserActionsPlugin:public FSLUserActionPlugin {
    
public:
    
    MyUserActionsPlugin();
    // virtual const G4VUserActionInitialization *getUserActionInitialization() const final;

    G4UserRunAction* getRunAction() const;
    G4UserSteppingAction* getSteppingAction() const;
};

MyUserActionsPlugin::MyUserActionsPlugin()
{
    std::cout<<"Hello from the MyUserActionsPlugin"<<std::endl;
}

G4UserRunAction* MyUserActionsPlugin::getRunAction() const
{
    std::cout << "G4UserRunAction inplementation of getRunAction method" << std::endl;
    return new MyRunAction();
}

G4UserSteppingAction* MyUserActionsPlugin::getSteppingAction() const
{
    std::cout << "G4UserRunAction inplementation of getSteppingAction method" << std::endl;
    return new MySteppingAction();
}

extern "C" MyUserActionsPlugin *createMyUserActionsPlugin() {
    return new MyUserActionsPlugin();
}

