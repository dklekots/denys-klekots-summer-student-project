This is the test FSL plugin for the custom physics list.

To build plugin, run in terminal

```
mkdir build
cd build
cmake ../
make -j
```
