
# Set up the version of GeoModel as a cache variable, so that other
# sub-projects could use this value.
set( GeoModelATLAS_VERSION "0.0.1" CACHE STRING
    "Version of the GeoModelATLAS project" )

